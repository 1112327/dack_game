﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class SettingForm : Form
    {
        public SettingForm()
        {
            LoadContent();
        }

        List<Sprite2D_Menu> sprites = new List<Sprite2D_Menu>();
        List<Texture2D> on, off;
        float ReadyTop = 200,ReadyTopExit = 550;
        bool isReady = false, isReadyExit = false;
        private void LoadContent()
        {
            CreateMenuBackgroundSprite(0, 0, "Menu\\background");
            CreateMenuButton(80, 530, "Menu\\music");
            CreateMenuButton(80, 600, "Menu\\sound");
            if (Global.isMusic)
                CreateMenuButton(300, 530, "Menu\\on");
            else CreateMenuButton(300, 530, "Menu\\off");
            if(Global.isSound) 
                CreateMenuButton(300, 600, "Menu\\on");
            else CreateMenuButton(300, 600, "Menu\\off");
            CreateMenuButton(300, 700, "Menu\\back");
            on = new List<Texture2D>();
            on.Add(Global.Content.Load<Texture2D>("Menu\\on"));
            off = new List<Texture2D>();
            off.Add(Global.Content.Load<Texture2D>("Menu\\off"));
        }

        private void CreateMenuButton(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D_Menu menuButtonSprite = new Sprite2D_Menu(left, top, textures);
            sprites.Add(menuButtonSprite);
        }

        private void CreateMenuBackgroundSprite(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D menuBackgroundSprite = new Sprite2D(left,top,textures);
            this.MainModel = menuBackgroundSprite;
            
        }
        public override void Update(GameTime gameTime)
        {
            if (isReady == false)
            {
                for (int i = 0; i < 5; i++)
                {
                    sprites[i].Top -= 3;
                }
                if (sprites[0].Top <= ReadyTop)
                {
                    isReady = true;
                }
            }
            if (isReadyExit == true)
            {
                for (int i = 0; i < 5; i++)
                {
                    sprites[i].Top += 3;
                }
                if (sprites[0].Top >= ReadyTopExit)
                {
                    isReadyExit = false;
                    isReady = false;
                    sprites[2].Select(false);
                    sprites[3].Select(false);
                    sprites[4].Select(false);
                    Global.SwitchToForm(2);
                }
            }
            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                // duplicated code ==> should be optimized!!!
                Vector2 MousePosition = TouchEventHelper.GetInstance().GetTouchPosition();
                Vector2 WorldPosition = Global.Screen2World(MousePosition);

                int idx = getSelectButtonIndex(WorldPosition);
                if (idx != -1)
                {
                    for (int i = 0; i < sprites.Count; i++)
                    {
                        sprites[i].Select(i == idx);
                        
                    }
                    
                    switch (idx) // chuyen man hinh
                    { 
                        case 2:
                            if (Global.isMusic == true)
                            {
                                Global.isMusic = false;
                                sprites[2].ListSprite1 = off;
                            }
                            else
                            {
                                Global.isMusic = true;
                                sprites[2].ListSprite1 = on;
                            }
                            break;
                        case 3:
                           if (Global.isSound == true)
                            {
                                Global.isSound = false;
                                sprites[3].ListSprite1 = off;
                            }
                            else
                            {
                                Global.isSound = true;
                                sprites[3].ListSprite1 = on;
                            }
                            break;
                        case 4:
                            isReadyExit = true;
                            break;
                    }
                    if (Global.isSound) SoundEffectHelper.GetInstance().playSound(1);
                }
            }
            else
             /*   if (TouchEventHelper.GetInstance().IsLeftButtonUp())
                {
                    Vector2 MousePosition = MouseEventHelper.GetInstance().GetMousePosition();
                    Vector2 WorldPosition = Global.Screen2World(MousePosition);

                    int idx = getSelectButtonIndex(WorldPosition);
                    if (idx != -1)
                    {
                        for (int i = 0; i < sprites.Count; i++)
                                sprites[i].Select(i == idx);
                        
                    }
            
                }*/
            base.Update(gameTime);  // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Update(gameTime);
        }

        private int getSelectButtonIndex(Vector2 WorldPosition)
        {
            int idx = -1;
            for (int i = 2; i < sprites.Count; i++)
                if (sprites[i].IsSelected(WorldPosition))
                {
                    idx = i;
                    break;
                }
            return idx;
        }
        public override void Draw(GameTime gameTime, SpriteBatch obj)
        {
            base.Draw(gameTime, obj); // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Draw(gameTime, obj);
            
        }
    }
}

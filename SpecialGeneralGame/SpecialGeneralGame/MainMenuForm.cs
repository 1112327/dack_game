﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace SpecialGeneralGame
{
    public class MainMenuForm : Form
    {
        public MainMenuForm()
        {
            LoadContent();
        }

        List<Sprite2D_Menu> sprites = new List<Sprite2D_Menu>();
        bool isReady = false, isReadyExit = false;
        float ReadyTop = 270;
        float ReadyExit = 550;
        int nextForm;

        private void LoadContent()
        {
            CreateMenuBackgroundSprite(0, 0, "Menu\\background");
            CreateMenuButton(300, 500, "Menu\\start");
            CreateMenuButton(120, 430, "Menu\\highscore");
            CreateMenuButton(130, 600, "Menu\\option");
            CreateMenuButton(520, 450, "Menu\\help");
            CreateMenuButton(530, 600, "Menu\\quit");
        }

        private void CreateMenuButton(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D_Menu menuButtonSprite = new Sprite2D_Menu(left, top, textures);
            sprites.Add(menuButtonSprite);
        }

        private void CreateMenuBackgroundSprite(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D menuBackgroundSprite = new Sprite2D(left,top,textures);
            this.MainModel = menuBackgroundSprite;
            
        }
        bool isWantExit = false, isQuitQuestion = false;
        float ReadyquitTopPos = 180;
        public void showQuit()
        {
            isQuitQuestion = true;
            isWantExit = true;
            List<Texture2D> tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\border_quit"));
            sprites.Add(new Sprite2D_Menu(220, 580, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\quit_question"));
            sprites.Add(new Sprite2D_Menu(350, 650, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\yes"));
            sprites.Add(new Sprite2D_Menu(280, 750, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\no"));
            sprites.Add(new Sprite2D_Menu(480, 750, tex));
        }
        public void xoaQuit()
        {
            isWantExit = false;
            for (int i = 0; i < 4; i++)
                sprites.Remove(sprites[sprites.Count - 1]);
            sprites[0].Select(true);
            isQuitQuestion = false;
            isWantExit = false;
        }
        int ind = 0, counttime = 180;
        public override void Update(GameTime gameTime)
        {
            
            if (isReady == false)
            {
                for (int i = 0; i < 5; i++)
                {
                    sprites[i].Top -= 3;
                }
                if (sprites[0].Top <= ReadyTop)
                {
                    ind = 0;
                    isReady = true;
                }
            }

            if (isReadyExit == true)
            {
                for (int i = 0; i < 5; i++)
                {
                    sprites[i].Top += 3;
                    ind = -1;
                }
                if (sprites[0].Top >= ReadyExit)
                {
                    isReady = false;
                    isReadyExit = false;
                    
                    Global.SwitchToForm(nextForm);
                }
            }
            if (isQuitQuestion == true)
            {
                for (int i = sprites.Count - 4; i < sprites.Count; i++)
                {
                    sprites[i].Top -= 3;
                    if (sprites[sprites.Count - 4].Top <= ReadyquitTopPos)
                        isQuitQuestion = false;
                }
            }
            if (isWantExit == false) counttime++;
            else {
                for (int i = 0; i < sprites.Count; i++)
                {
                    sprites[i].Select(i == -1);
                }
            }
            if ((counttime == 300 || counttime ==-1) && isWantExit == false)
            {
                
                if (ind == 5) ind = 0;
                
                counttime = 0;
                for (int i = 0; i < sprites.Count; i++)
                {
                    sprites[i].Select(i == ind);
                }
                ind++;
            }
            
            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                // duplicated code ==> should be optimized!!!
                Vector2 MousePosition = TouchEventHelper.GetInstance().GetTouchPosition();
                Vector2 WorldPosition = Global.Screen2World(MousePosition);

                int idx = getSelectButtonIndex(WorldPosition);
                if (idx != -1)
                {
                    for (int i = 0; i < sprites.Count; i++)
                    {
                        sprites[i].Select(i == idx);
                        
                    }
                    ind = -1;
                    if (Global.isSound) SoundEffectHelper.GetInstance().playSound(1);
                    switch (idx) // chuyen man hinh
                    { 
                            
                        case 0: //start
                            isReadyExit = true;
                            nextForm = 4;
                            break;
                        case 1:  // highscore
                            break;
                        case 2: // option
                            isReadyExit = true;
                            
                            nextForm = 3;
                            break;
                        case 3: //help
                            Global.SwitchToForm(5);
                            break;
                        case 4: // quit
                            showQuit();
                            break;

                        case 7:
                            Global.bQuit = true;
                            break;
                        case 8:
                            xoaQuit();
                            break;
                        
                    }
                }
            }
            else
              /*  if (MouseEventHelper.GetInstance().IsLeftButtonUp())
                {
                    Vector2 MousePosition = MouseEventHelper.GetInstance().GetMousePosition();
                    Vector2 WorldPosition = Global.Screen2World(MousePosition);

                    int idx = getSelectButtonIndex(WorldPosition);
                    if (idx != -1)
                    {
                        for (int i = 0; i < sprites.Count; i++)
                                sprites[i].Select(i == idx);
                        
                    }
            
                }*/
            base.Update(gameTime);  // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Update(gameTime);
        }

        private int getSelectButtonIndex(Vector2 WorldPosition)
        {
            int idx = -1;
            if (isWantExit == false)
            {
                for (int i = 0; i < sprites.Count; i++)
                    if (sprites[i].IsSelected(WorldPosition))
                    {
                        idx = i;
                        break;
                    }
            }
            else
                for (int i = sprites.Count - 2; i < sprites.Count; i++)
                    if (sprites[i].IsSelected(WorldPosition))
                    {
                        idx = i;
                        break;
                    }
            return idx;
        }
        public override void Draw(GameTime gameTime, SpriteBatch obj)
        {
            base.Draw(gameTime, obj); // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Draw(gameTime, obj);
            
        }
    }
}

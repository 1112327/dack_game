﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class Sprite2D
    {
        float _left, _top, _depth = 0, _scale = 1, count = -1, _rotation = 0;
        bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set { _isValid = value; }
        }

        public float Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
        int    _height, _width ;
        public float Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }
        List<Texture2D> _ListSprite;

        public List<Texture2D> ListSprite1
        {
            get { return _ListSprite; }
            set { _ListSprite = value;
            _nSprite = _ListSprite.Count;
            _curSprite = 0;
            _width = _ListSprite[0].Width;
            _height = _ListSprite[0].Height;
            }
        }
        int _nSprite, _curSprite, _state = 0;

        public int State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int CurSprite
        {
            get { return _curSprite; }
            set { _curSprite = value; }
        }
       

        public int NSprite
        {
            get { return _nSprite; }
            set { _nSprite = value; }
        }
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public float Top
        {
            get { return _top; }
            set { _top = value; }
        }

        public float Left
        {
            get { return _left; }
            set { _left = value; }
        }
        public Sprite2D(float left, float top, List<Texture2D> list)
        {
            Left = left;
            Top = top;
            Height = list[0].Height;
            Width = list[0].Width;
            CurSprite = 0;
            NSprite = list.Count;
            _ListSprite = list;
        }
        public  virtual bool IsSelected(Vector2 vitri)
        {
            
            if (vitri.X >= Left && vitri.X <= Left + Width && vitri.Y >= Top && vitri.Y <= Top + Height)
                return true;
            return false;
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spritebatch)
        {
            InternalDraw(gameTime, spritebatch);
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spritebatch, Color color)
        {
            InternalDraw(gameTime, spritebatch, color);
        }
        private void InternalDraw(GameTime gameTime, SpriteBatch spritebatch,Color color)
        {
            spritebatch.Draw(_ListSprite[CurSprite], new Vector2(Left, Top), null, color, 0, Vector2.Zero, Scale, SpriteEffects.None, Depth);
        }
        private void InternalDraw(GameTime gameTime, SpriteBatch spritebatch)
        {

            if (State == 0)
                // spritebatch.Draw(ListSprite[CurSprite], new Vector2(Left, Top), Color.White);
                spritebatch.Draw(_ListSprite[CurSprite], new Vector2(Left, Top), null, Color.White, 0, Vector2.Zero, Scale, SpriteEffects.None, Depth);
            else
            {
                if (count > 0)
                {
                    spritebatch.Draw(_ListSprite[CurSprite], new Vector2(Left, Top), null, Color.LightGray, 0, Vector2.Zero, Scale, SpriteEffects.None, Depth);
                    count--;
                }
                else State = 0;
            }
        }
        int d1 = 0;
        int d2 = 1;
        public virtual void Update(GameTime gameTime)
        {
            CurSprite = (CurSprite + 1) % NSprite;
            if (State == 1)
            {
                if (Math.Abs(d1) == 10)
                    d2 *= -1;
                d1 += d2;
            }
        }
        public virtual void Select(bool bSelected)
        {
            if (bSelected)
            {
                State = 1;
                count = 10;
                _rotation = 0;
            }
            else
            {
                State = 0;
                count = 0;
            }
        }
    }
}

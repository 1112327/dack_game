﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public abstract class Enemy : VisibleGameEntity
    {
        float _maxhealth;
        public int countdown_slow = -1, slowdown_time = 3;
        public float Maxhealth
        {
            get { return _maxhealth; }
            set { _maxhealth = value; }
        }
        float _health, _speed, _damage, _type;

        public float Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public float Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public float Health
        {
            get { return _health; }
            set { _health = value; }
        }
        protected List<Texture2D> walking, fight, die;
        public enum position_state
        { 
            up,mid,down
        };
        private position_state _position_State;

        public position_state Position_State
        {
            get { return _position_State; }
            set { _position_State = value; }
        }
        public enum state
        { 
            walking,fight,die
        };
        private state _state;

        public state State
        {
            get { return _state; }
            set { _state = value;
            switch (value)
            { 
                case state.walking:
                    MainModel.ListSprite1 = walking;
                    break;
                case state.fight:
                    MainModel.ListSprite1 = fight;
                    break;
                case state.die:
                    MainModel.ListSprite1 = die;
                    break;
            }
            }
        }
        Texture2D _healthBar;
        public Texture2D HealthBar
        {
            get { return _healthBar; }
            set { _healthBar = value; }
        }
        protected bool isEnemy = false;
        public Enemy(bool isEnemy, Vector2 pos, position_state posstate)
        {
            walking = new List<Texture2D>();
            fight = new List<Texture2D>();
            die = new List<Texture2D>();
            this.isEnemy = isEnemy;
            HealthBar = Global.Content.Load<Texture2D>("Scenario\\HealthBar");
            Position_State = posstate;
        }

        public Enemy(Enemy enemy)
        {
            // TODO: Complete member initialization
            Health = enemy.Health;
            Speed = enemy.Speed;
            Damage = enemy.Damage;
            Type = enemy.Type;
            isEnemy = enemy.isEnemy;
            HealthBar = enemy.HealthBar;
            Maxhealth = enemy.Maxhealth;
            walking = enemy.walking;
            fight = enemy.fight;
            die = enemy.die;
            deadcount = enemy.deadcount;
            fightcount = enemy.fightcount;
            this.MainModel = new Sprite2D(enemy.MainModel.Left, enemy.MainModel.Top, enemy.MainModel.ListSprite1);
        }

        public virtual Enemy Clone()
        {
            return null;
        }
        protected int deadcount, fightcount;
        public override void Update(GameTime gameTime)
        {
            
            

            base.Update(gameTime);
            int ind = TargetEnemy(isEnemy);
            switch(State)
            {
                case state.walking:
                    if (isEnemy)
                    {
                        if (MainModel.Left >= 0)
                            MainModel.Left -= Speed;
                    }
                    else
                    {
                        if (MainModel.Left <= 1400)
                            MainModel.Left += Speed;
                    }
                break;
                case state.die:
                    deadcount--;
                    if (deadcount <= 0)
                    Global.CurrentMap.DeadEnemy.Remove(this);
                    break;
                case state.fight:
                    fightcount--;
                    if (fightcount <= 0)
                    {
                        if (ind != -1 && ind != -2)
                        {
                            switch(Position_State)
                            {
                                case position_state.up:
                                    {
                                        if (isEnemy)
                                            Global.CurrentMap.myEnemyUp[ind].TakeDamage(Damage);
                                        else Global.CurrentMap.EnemyUp[ind].TakeDamage(Damage);
                                    }
                                    break;
                                case position_state.mid:
                                    {
                                        if (isEnemy)
                                            Global.CurrentMap.myEnemy[ind].TakeDamage(Damage);
                                        else Global.CurrentMap.Enemy[ind].TakeDamage(Damage);
                                    }
                                    break;
                                case position_state.down:
                                    {
                                        if (isEnemy)
                                            Global.CurrentMap.myEnemyDown[ind].TakeDamage(Damage);
                                        else Global.CurrentMap.EnemyDown[ind].TakeDamage(Damage);
                                    }
                                    break;
                            }   
                            fightcount = fight.Count;
                        }
                        else
                        {
                            if (ind == -2)
                            {
                                if (isEnemy) Global.CurrentMap.MainHouse.TakeDamage(Damage);
                                else Global.CurrentMap.MainHouse_Enemy.TakeDamage(Damage);
                                fightcount = fight.Count;
                            }
                            else State = state.walking;
                        }
                    }
                    break;
            }
            if (State == state.walking)
            {
                ind = TargetEnemy(isEnemy);
                if (ind != -1)
                {
                    State = state.fight;
                }
                
            }

           // Health -= 0.2f;
            if (Health < 0)
                Health = 0;
        }
        public void SlowDown(int time)
        {
            countdown_slow = time;
        }
        private int TargetEnemy(bool isenemy)
        {
            if (isenemy)
            {
                if (MainModel.Left + 20 <= Global.CurrentMap.MainHouse.MainModel.Left + Global.CurrentMap.MainHouse.MainModel.Width)
                    return -2;

            }
            else
            {
                if (MainModel.Left + MainModel.Width - 20 >= Global.CurrentMap.MainHouse_Enemy.MainModel.Left)
                    return -2;
            }
            switch(Position_State)
            {
                case position_state.mid:
                    {
                        if (isenemy)
                        {
                            for (int i = 0; i <= Global.CurrentMap.myEnemy.Count - 1; i++)
                                if (Global.CurrentMap.myEnemy[i] != null)
                                {
                                    if (MainModel.Left + 20 <= Global.CurrentMap.myEnemy[i].MainModel.Left + Global.CurrentMap.myEnemy[i].MainModel.Width)
                                        return i;
                                }
                        }
                        else
                        {
                            for (int i = 0; i <= Global.CurrentMap.Enemy.Count - 1; i++)
                                if (Global.CurrentMap.Enemy[i] != null)
                                {
                                    if (MainModel.Left + MainModel.Width - 20 >= Global.CurrentMap.Enemy[i].MainModel.Left)
                                        return i;
                                }
                        }
                    }
                    break;
                case position_state.up:
                    {
                        if (isenemy)
                        {
                            for (int i = 0; i <= Global.CurrentMap.myEnemyUp.Count - 1; i++)
                                if (Global.CurrentMap.myEnemyUp[i] != null)
                                {
                                    if (MainModel.Left + 20 <= Global.CurrentMap.myEnemyUp[i].MainModel.Left + Global.CurrentMap.myEnemyUp[i].MainModel.Width)
                                        return i;
                                }
                        }
                        else
                        {
                            for (int i = 0; i <= Global.CurrentMap.EnemyUp.Count - 1; i++)
                                if (Global.CurrentMap.EnemyUp[i] != null)
                                {
                                    if (MainModel.Left + MainModel.Width - 20 >= Global.CurrentMap.EnemyUp[i].MainModel.Left)
                                        return i;
                                }
                        }
                    }
                    break;
                case position_state.down:
                    {
                        if (isenemy)
                        {
                            for (int i = 0; i <= Global.CurrentMap.myEnemyDown.Count - 1; i++)
                                if (Global.CurrentMap.myEnemyDown[i] != null)
                                {
                                    if (MainModel.Left + 20 <= Global.CurrentMap.myEnemyDown[i].MainModel.Left + Global.CurrentMap.myEnemyDown[i].MainModel.Width)
                                        return i;
                                }
                        }
                        else
                        {
                            for (int i = 0; i <= Global.CurrentMap.EnemyDown.Count - 1; i++)
                                if (Global.CurrentMap.EnemyDown[i] != null)
                                {
                                    if (MainModel.Left + MainModel.Width - 20 >= Global.CurrentMap.EnemyDown[i].MainModel.Left)
                                        return i;
                                }
                        }
                    }

                    break;
        }
            return -1;
        }
        public void TakeDamage(float damg)
        {
            Health -= damg;
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            //base.Draw(gameTime, spriteBatch);
            if (countdown_slow > 0)
            {
                MainModel.Draw(gameTime, spriteBatch, Color.BlueViolet);
            }
            else MainModel.Draw(gameTime, spriteBatch);
            if (State != state.die)
            {
                if (Health <= 0)
                {
                    switch (Position_State)
                    {
                        case position_state.mid:
                            {
                                if (isEnemy)
                                {
                                    Global.CurrentMap.Enemy.Remove(this);
                                }
                                else
                                {
                                    Global.CurrentMap.myEnemy.Remove(this);
                                }
                            }
                            break;
                        case position_state.up:
                            {
                                if (isEnemy)
                                {
                                    Global.CurrentMap.EnemyUp.Remove(this);
                                }
                                else
                                {
                                    Global.CurrentMap.myEnemyUp.Remove(this);
                                }
                            }
                            break;
                        case position_state.down:
                            {
                                if (isEnemy)
                                {
                                    Global.CurrentMap.EnemyDown.Remove(this);
                                }
                                else
                                {
                                    Global.CurrentMap.myEnemyDown.Remove(this);
                                }
                            }
                            break;
                    }
                    State = state.die;
                    Global.CurrentMap.DeadEnemy.Add(this);
                    ((GameForm)Global.CurrentForm).Score++;
                }
                else
                {
                    spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 13), ((int)MainModel.Top - 20), HealthBar.Width, HealthBar.Height), Color.Gray);
                    if (Health / Maxhealth <= 0.25)
                        spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 13), ((int)MainModel.Top - 20), (int)(HealthBar.Width * Health / Maxhealth), HealthBar.Height), Color.Red);
                    else spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 13), ((int)MainModel.Top - 20), (int)(HealthBar.Width * Health / Maxhealth), HealthBar.Height), Color.Green);
                }
            }
        }

        
    }
}

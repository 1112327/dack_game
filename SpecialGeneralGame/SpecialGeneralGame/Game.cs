using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace SpecialGeneralGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int normalSpeed = 166667;
        int fastSpeed = 166667 / 2;
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(166667);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);

            //graphics.PreferredBackBufferWidth = 800;
           // graphics.PreferredBackBufferHeight = 480;
            
        }
        public void setElapsedTime(long time)
        {
            TargetElapsedTime = TimeSpan.FromTicks(time);
        }
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            Global.graphics = GraphicsDevice;
            Global.Content = this.Content;
            CreateAllForms();
            ((WinForm)Global.GetForm(8)).updateData();
            Global.SwitchToForm(8);
            Global.CurrentCamera = new Camera(1f, 1f, 0, 0, 0);
            Global.CurrentCamera.Pathupdater = new IdlePathUpdater(0, 0);
            Global.SubCamera = new Camera(1, 1, 0, 0, 0);
            Global.SubCamera.Pathupdater = new IdlePathUpdater(0, 0);
            Global.DefaultCamera = Global.CurrentCamera;
            
            // TODO: use this.Content to load your game content here
        }
        private void CreateAllForms()
        {
            
            CreateMainForm(2);
            CreateMainForm(3);
            CreateMainForm(4);
            CreateMainForm(7);
            CreateMainForm(8);
            CreateMainForm(9);
            //CreateGameForm(1, "Scenario1.xml");
           
        }
        private void CreateMainForm(int FormID)
        {
            switch (FormID)
            {
                case 0:
                    Form form = new IntroduceForm();
                    form.ID = FormID;
                    Global.Forms.Add(form);
                    break;
                case 1:

                    break;
                case 2:
                    Form form2 = new MainMenuForm();
                    form2.ID = FormID;
                    Global.Forms.Add(form2);
                    break;
                case 3:
                    Form form3 = new SettingForm();
                    form3.ID = FormID;
                    Global.Forms.Add(form3);
                    break;
                case 4:
                    Form form4 = new SelectModeForm();
                    form4.ID = FormID;
                    Global.Forms.Add(form4);
                    break;
                case 5: //chon map hero
                    break;
                case 6: //chon map boss
                    break;
                case 7: // game form
                    Form form7 = new GameForm(0);
                    form7.ID = FormID;
                    Global.Forms.Add(form7);
                    break;
                case 8: //win form
                    Form form8 = new WinForm();
                    form8.ID = FormID;
                    Global.Forms.Add(form8);
                    break;
                case 9:
                    Form form9 = new LoseForm();
                    form9.ID = FormID;
                    Global.Forms.Add(form9);
                    break;
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        bool bDrag = false;
        protected override void Update(GameTime gameTime)
        {
            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            switch(Global.CurrentForm.ID)
            {
                case 2: // main menu

                     if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Global.bQuit == true)
                                this.Exit();
                break;

                case 7: // main game
                {
                    if (((GameForm)Global.CurrentForm).StateGame == GameForm.gamestate.pause || ((GameForm)Global.CurrentForm).StateGame == GameForm.gamestate.win
                        || ((GameForm)Global.CurrentForm).StateGame == GameForm.gamestate.lose || ((GameForm)Global.CurrentForm).StateGame == GameForm.gamestate.turnback)
                    {
                        ((GameForm)Global.CurrentForm).Update(gameTime);
                    }
                    else
                    {
                        TouchEventHelper.GetInstance().ProcessNewState(TouchPanel.GetState(), TouchPanel.GetCapabilities());
                        Vector2 ScreenPos = TouchEventHelper.GetInstance().GetTouchPosition();
                        Vector2 WorldPos = Vector2.Transform(ScreenPos, Global.CurrentCamera.InvWVPMatrix);
                        if (TouchEventHelper.GetInstance().HasTouchDownEvent())
                        {
                            int ind = ((GameForm)Global.CurrentForm).getStatePos(ScreenPos);
                            if (ind > -1) // chon trung duong, cbi mua linh
                            {
                                ((GameForm)Global.CurrentForm).ShowSelectCoin(ind);
                            }
                            else
                            {
                                bDrag = true;
                                if (((GameForm)Global.CurrentForm).SelectStatePos > -1) // kiem tra da chon duong di
                                {
                                    int type = ((GameForm)Global.CurrentForm).getSelectCoin(ScreenPos);
                                    if (type > -1) // neu chon trung loai tinh thi bat dau mua linh
                                    {
                                        ((GameForm)Global.CurrentForm).CountdownTarget(type, ((GameForm)Global.CurrentForm).SelectStatePos);

                                    }
                                    else ((GameForm)Global.CurrentForm).SelectStatePos = -1;
                                }
                            }
                            //kiem tra nang cap quai
                            ind = ((GameForm)Global.CurrentForm).getUpgradePos(ScreenPos);
                            if (ind > -1)
                            {
                                ((GameForm)Global.CurrentForm).upgradeEnemy(ind);
                            }
                            // kiem tra su dung skill ho tro
                            ind = ((GameForm)Global.CurrentForm).getSkillPos(ScreenPos);
                            if (ind > -1)
                            {
                                ((GameForm)Global.CurrentForm).ExcuteSkill(ind);
                            }
                            //kiem tra run fast
                            ind = ((GameForm)Global.CurrentForm).getFastPos(ScreenPos);
                            if (ind > -1)
                            {
                                if (ind == 0)
                                {
                                    TargetElapsedTime = TimeSpan.FromTicks(fastSpeed);
                                    ((GameForm)Global.CurrentForm).SetGameFast(true);
                                }
                                if (ind == 1)
                                {
                                    TargetElapsedTime = TimeSpan.FromTicks(normalSpeed);
                                    ((GameForm)Global.CurrentForm).SetGameFast(false);
                                }
                            }
                            if (((GameForm)Global.CurrentForm).isPause(ScreenPos))
                            {
                                ((GameForm)Global.CurrentForm).pauseGame();
                            }
                        }
                        else if (TouchEventHelper.GetInstance().HasTouchUpEvent())
                        {
                            Vector2 tran = TouchEventHelper.GetInstance().GetTouchPosDifference();
                            Vector2 Worlpos = Vector2.Transform(new Vector2(0, 0), Global.CurrentCamera.InvWVPMatrix);
                            Global.CurrentCamera.XTran -= tran.X;
                            Global.SubCamera.XTran += tran.X;
                            Global.SubCamera.XTran += tran.X;
                            if (Global.CurrentCamera.XTran > 0)
                                Global.CurrentCamera.XTran = 0;
                            if (Global.SubCamera.XTran < 0)
                                Global.SubCamera.XTran = 0;
                            if (Global.CurrentCamera.XTran < -800)
                                Global.CurrentCamera.XTran = -800;
                            if (Global.SubCamera.XTran > 800)
                                Global.SubCamera.XTran = 800;
                            // Global.CurrentCamera.YTran -= tran.Y;
                            bDrag = false;
                        }
                        else if (TouchEventHelper.GetInstance().HasMoveDownEvent())
                        {
                            if (bDrag)
                            {
                                Vector2 tran = TouchEventHelper.GetInstance().GetTouchPosDifference();
                                Vector2 Worlpos = Vector2.Transform(new Vector2(0, 0), Global.CurrentCamera.InvWVPMatrix);
                                //if (WorldPos.X > 0)
                                Global.CurrentCamera.XTran -= tran.X;
                                Global.SubCamera.XTran += tran.X;
                                if (Global.CurrentCamera.XTran > 0)
                                    Global.CurrentCamera.XTran = 0;
                                if (Global.SubCamera.XTran < 0)
                                    Global.SubCamera.XTran = 0;
                                if (Global.CurrentCamera.XTran < -800)
                                    Global.CurrentCamera.XTran = -800;
                                if (Global.SubCamera.XTran > 800)
                                    Global.SubCamera.XTran = 800;
                                //Global.CurrentCamera.YTran -= tran.Y;
                            }
                        }
                    }
                }
                break;
                case 8: //win form
                {
                    Global.CurrentForm.Update(gameTime);
                }
                break;
                case 9:
                    {
                        Global.CurrentForm.Update(gameTime);
                    }
                    break;
            }


            // TODO: Add your update logic here

            base.Update(gameTime);
            if (Global.CurrentCamera != null)
                Global.CurrentCamera.Update(gameTime);
            if (Global.CurrentForm != null)
                Global.CurrentForm.Update(gameTime);
            TouchEventHelper.GetInstance().Update(gameTime);
            SoundEffectHelper.GetInstance().Update(gameTime);
            BackgroundMusicHelper.GetInstance().Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Aqua);

            // TODO: Add your drawing code here

            base.Draw(gameTime);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, Global.CurrentCamera.WVPMatrix);
            if (Global.CurrentForm != null)
                Global.CurrentForm.Draw(gameTime, spriteBatch);
            spriteBatch.End();
            


        }
    }
}

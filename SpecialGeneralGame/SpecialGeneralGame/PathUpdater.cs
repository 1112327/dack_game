﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public abstract class PathUpdater
    {
        public virtual Vector2 GetCurrentPosition()
        {
            return Vector2.Zero;
        }
        public virtual void Update(GameTime gameTime)
        { }
    }
}

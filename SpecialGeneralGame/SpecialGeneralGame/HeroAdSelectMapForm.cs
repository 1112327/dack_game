﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class HeroAdSelectMapForm : Form
    {
        List<Sprite2D_Menu> sprites = new List<Sprite2D_Menu>();
        float ReadyLeft = 180, ReadyLeftExit = -200;
        bool isReady = false, isReadyExit = false;
        public HeroAdSelectMapForm()
        {
            LoadContent();
        }
        private void LoadContent()
        {
            CreateMenuBackgroundSprite(0, 0, "Menu\\background");
            CreateMenuButton(-200, 180, "Menu\\hero_icon");
            CreateMenuButton(800, 180, "Menu\\boss_icon");
            CreateMenuButton(280, 770, "Menu\\back");
        }

        private void CreateMenuButton(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D_Menu menuButtonSprite = new Sprite2D_Menu(left, top, textures);
            sprites.Add(menuButtonSprite);
        }

        private void CreateMenuBackgroundSprite(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D menuBackgroundSprite = new Sprite2D(left, top, textures);
            this.MainModel = menuBackgroundSprite;

        }
        int nextForm;
        public override void Update(GameTime gameTime)
        {
            if (isReady == false)
            {
                sprites[0].Left += 3;
                sprites[1].Left -= 3;
                sprites[2].Top -= 3;
                if (sprites[0].Left >= ReadyLeft)
                {
                    isReady = true;
                }
            }
            if (isReadyExit == true)
            {
                sprites[0].Left -= 3;
                sprites[1].Left += 3;
                sprites[2].Top += 3;
                if (sprites[0].Left <= ReadyLeftExit)
                {
                    isReadyExit = false;
                    isReady = false;
                    sprites[0].Select(false);
                    sprites[1].Select(false);
                    sprites[2].Select(false);
                    Global.SwitchToForm(nextForm);
                }
            }
            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                // duplicated code ==> should be optimized!!!
                Vector2 MousePosition = TouchEventHelper.GetInstance().GetTouchPosition();
                Vector2 WorldPosition = Global.Screen2World(MousePosition);

                int idx = getSelectButtonIndex(WorldPosition);
                if (idx != -1)
                {
                    for (int i = 0; i < sprites.Count; i++)
                    {
                        sprites[i].Select(i == idx);

                    }

                    switch (idx) // chuyen man hinh
                    {
                        case 0:
                            
                            break;
                        case 1:
                           
                            break;
                        case 2:
                            isReadyExit = true;
                            nextForm = 2;
                            break;
                    }
                    if (Global.isSound) SoundEffectHelper.GetInstance().playSound(1);
                }
            }
            else
                /*   if (TouchEventHelper.GetInstance().IsLeftButtonUp())
                   {
                       Vector2 MousePosition = MouseEventHelper.GetInstance().GetMousePosition();
                       Vector2 WorldPosition = Global.Screen2World(MousePosition);

                       int idx = getSelectButtonIndex(WorldPosition);
                       if (idx != -1)
                       {
                           for (int i = 0; i < sprites.Count; i++)
                                   sprites[i].Select(i == idx);
                        
                       }
            
                   }*/
                base.Update(gameTime);  // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Update(gameTime);
        }
        private int getSelectButtonIndex(Vector2 WorldPosition)
        {
            int idx = -1;
            for (int i = 2; i < sprites.Count; i++)
                if (sprites[i].IsSelected(WorldPosition))
                {
                    idx = i;
                    break;
                }
            return idx;
        }
        public override void Draw(GameTime gameTime, SpriteBatch obj)
        {
            base.Draw(gameTime, obj); // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Draw(gameTime, obj);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;


namespace SpecialGeneralGame
{
    public class GameForm : Form
    {
        Sprite2D CoinBar_Background, PauseSprite;
        List<Sprite2D> List_Coin = new List<Sprite2D>();
        List<Sprite2D> List_StatePos = new List<Sprite2D>();
        List<Sprite2D> List_UpGrade = new List<Sprite2D>();
        List<Sprite2D> List_Skill_Icon = new List<Sprite2D>();
        List<Sprite2D> List_Skill = new List<Sprite2D>();
        List<Sprite2D> List_Fast = new List<Sprite2D>();
        List<Sprite2D> List_PauseGame = new List<Sprite2D>();
        Texture2D CoinBar, CountDownBar;
        float _coin, _maxcoin;
        
        public int countdownpos, countdownpos_select, countdown_iceskill, countdown_windskill, Skill_countdown, Skill_Target, TotalTime = 0, Score = 0;
        public float CoinSpeedUp = 0.5f;
        List<int> target_list = new List<int>();
        List<int> time_countselect = new List<int>();
        List<int> time_countselectMax = new List<int>();
        List<int> TargetIndex = new List<int>();
        List<int> TargetPosState = new List<int>();
        public enum gamestate
        { 
            playing, pause, forward, win,lose, turnback
        }
        gamestate _stateGame;

        public gamestate StateGame
        {
            get { return _stateGame; }
            set { _stateGame = value;
            switch (StateGame)
            { 
                case gamestate.turnback:
                    if (List_PauseGame.Count <= 5)
                    {
                        List<Texture2D> tex = new List<Texture2D>();
                        tex.Add(Global.Content.Load<Texture2D>("Scenario\\menureturn"));
                        List_PauseGame.Add(new Sprite2D(100, 150, tex));
                        tex = new List<Texture2D>();
                        tex.Add(Global.Content.Load<Texture2D>("Menu\\yes"));
                        List_PauseGame.Add(new Sprite2D(180, 230, tex));
                        tex = new List<Texture2D>();
                        tex.Add(Global.Content.Load<Texture2D>("Menu\\no"));
                        List_PauseGame.Add(new Sprite2D(280, 230, tex));
                    }
                    break;
                case gamestate.pause:
                    while (List_PauseGame.Count > 5)
                        List_PauseGame.RemoveAt(List_PauseGame.Count - 1);
                    break;
            }
            }
        }
        public float Maxcoin
        {
            get { return _maxcoin; }
            set { _maxcoin = value; }
        }

        public float Coin
        {
            get { return _coin; }
            set { _coin = value; }
        }
        public int SelectStatePos = -1;
        public GameForm(int mapNum)
        {
            Global.CurrentMap = new Map(mapNum, "Data\\MapData");
            List<Texture2D> tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\CoinBar_Background"));
            CoinBar_Background = new Sprite2D(180, 430, tex);
            CoinBar = Global.Content.Load<Texture2D>("Scenario\\CoinBar");

            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Up_Choose1"));
            List_StatePos.Add(new Sprite2D(420, 450, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Mid_Choose1"));
            List_StatePos.Add(new Sprite2D(460, 450, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Down_Choose1"));
            List_StatePos.Add(new Sprite2D(500, 450, tex));

            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Enemy_coin1"));
            List_Coin.Add(new Sprite2D(450, 525, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Enemy_coin2"));
            List_Coin.Add(new Sprite2D(500, 525, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Enemy_coin3"));
            List_Coin.Add(new Sprite2D(550, 525, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\Enemy_coin4"));
            List_Coin.Add(new Sprite2D(600, 525, tex));
            Maxcoin = 2000;
            Coin = 2000;
            CountDownBar = Global.Content.Load<Texture2D>("Scenario\\countdownbar");

            
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\enemy1_up"));
            List_UpGrade.Add(new Sprite2D(450, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\enemy2_up"));
            List_UpGrade.Add(new Sprite2D(500, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\enemy3_up"));
            List_UpGrade.Add(new Sprite2D(550, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\enemy4_up"));
            List_UpGrade.Add(new Sprite2D(600, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\time_up"));
            List_UpGrade.Add(new Sprite2D(400, 00, tex));

            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\WindSkill_icon"));
            List_Skill_Icon.Add(new Sprite2D(250, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\IceSkill_icon"));
            List_Skill_Icon.Add(new Sprite2D(300, 00, tex));

            tex = new List<Texture2D>();
            for (int i = 1; i <= 17; i++)
            {
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Wind_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Wind_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Wind_Effect" + i.ToString("00")));
            }
            List_Skill.Add(new Sprite2D(100, 50, tex));
            tex = new List<Texture2D>();
            for (int i = 1; i <= 8; i++)
            {
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Ice_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Ice_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Ice_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Ice_Effect" + i.ToString("00")));
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\Skill\\Ice_Effect" + i.ToString("00")));
            }
            List_Skill.Add(new Sprite2D(100, 50, tex));

            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\forward"));
            List_Fast.Add(new Sprite2D(250, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\backward"));
            List_Fast.Add(new Sprite2D(250, 00, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Scenario\\pause"));
            PauseSprite = new Sprite2D(750, 0, tex);

            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\pausegame"));
            List_PauseGame.Add(new Sprite2D(150, 50, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\tieptuc"));
            List_PauseGame.Add(new Sprite2D(200, 150, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\menu"));
            List_PauseGame.Add(new Sprite2D(230, 320, tex));
            tex = new List<Texture2D>();
            if (Global.isMusic)
                tex.Add(Global.Content.Load<Texture2D>("Menu\\musicon"));
            else tex.Add(Global.Content.Load<Texture2D>("Menu\\musicoff"));
            List_PauseGame.Add(new Sprite2D(250, 220, tex));
            tex = new List<Texture2D>();
            if (Global.isMusic)
                tex.Add(Global.Content.Load<Texture2D>("Menu\\soundon"));
            else tex.Add(Global.Content.Load<Texture2D>("Menu\\soundoff"));
            List_PauseGame.Add(new Sprite2D(350, 220, tex));

        }
        public void buyEnemy(int type)
        {
            Global.CurrentMap.buyEnemy(type);
            switch (type)
            { 
                case 0:
                    Coin -= 200;
                    break;
                case 1:
                    Coin -= 500;
                    break;
                case 2:
                    Coin -= 700;
                    break;
                case 3:
                    Coin -= 1000;
                    break;
            }
        }

        public void buyEnemy(int type, int state)
        {
            CountdownTarget(type,state);      
        }

        

        public override void Update(GameTime gameTime)
        {
            if (StateGame != gamestate.pause && StateGame != gamestate.turnback)
            {
                //base.Update(gameTime);
                if (Global.CurrentMap != null)
                    Global.CurrentMap.Update(gameTime);
                TotalTime++;
                
                if (Coin <= Maxcoin)
                    Coin += CoinSpeedUp;
                else Coin = Maxcoin;
                if (Coin < 200)
                    List_Coin[0].IsValid = false;
                else List_Coin[0].IsValid = true;
                if (Coin < 500)
                    List_Coin[1].IsValid = false;
                else List_Coin[1].IsValid = true;
                if (Coin < 700)
                    List_Coin[2].IsValid = false;
                else List_Coin[2].IsValid = true;
                if (Coin < 1000)
                    List_Coin[3].IsValid = false;
                else List_Coin[3].IsValid = true;

                if (SelectStatePos == -1)
                    for (int i = 0; i < List_Coin.Count; i++)
                        List_Coin[i].Top = 525;
                for (int i = 0; i < time_countselect.Count; i++)
                {
                    if (time_countselect[i] > 0)
                        time_countselect[i]--;
                    if (time_countselect[i] == 0)
                    {
                        Global.CurrentMap.buyEnemy(TargetIndex[i], TargetPosState[i]);
                        time_countselect.RemoveAt(i);
                        time_countselectMax.RemoveAt(i);
                        TargetIndex.RemoveAt(i);
                        TargetPosState.RemoveAt(i);

                    }
                }
                if (Skill_Target == 0)
                    List_Skill[0].Update(gameTime);
                if (Skill_Target == 1)
                    List_Skill[1].Update(gameTime);


                if (countdown_iceskill > 0)
                    countdown_iceskill--;
                if (countdown_windskill > 0)
                    countdown_windskill--;
                if (Skill_countdown > 0)
                    Skill_countdown--;
                if (Skill_countdown == 0)
                {
                    Global.CurrentMap.ExcuteSkill(Skill_Target);
                    Skill_countdown = -1;
                    Skill_Target = -1;
                }

                if (StateGame == gamestate.playing)
                {
                    List_Fast[0].Left = 700;
                    List_Fast[1].Left = 800;
                }
                else if (StateGame == gamestate.forward)
                {
                    List_Fast[1].Left = 700;
                    List_Fast[0].Left = 800;
                }
            }
            else
            {
                TouchEventHelper.GetInstance().ProcessNewState(TouchPanel.GetState(), TouchPanel.GetCapabilities());
                Vector2 ScreenPos = TouchEventHelper.GetInstance().GetTouchPosition();
                if (TouchEventHelper.GetInstance().HasTouchDownEvent())
                {
                    int ind = getPausePos(ScreenPos);
                    switch (ind)
                    { 
                        case 1: // tiep tuc
                            resumeGame();
                            break;
                        case 2: // ve menu
                            StateGame = gamestate.turnback;
                            break;
                        case 3: // music
                            break;
                        case 4: // sound
                            break;
                        case 6: // yes
                            break;
                        case 7: // no
                            StateGame = gamestate.pause;
                            break;
                    }
                }
                else
                { 
                    
                }
            }
        }
        private void showPauseMenu()
        { 
            
        }
        public void SetGameFast(bool isFast)
        {
            if (isFast)
            {
                StateGame = gamestate.forward;
            }
            else
            {
                StateGame = gamestate.playing;
            }
        }
        public void ShowSelectCoin(int ind) // đầu vào là một vị trí đường: trên, giữa, dưới
        {
            for (int i = 0; i < List_Coin.Count; i++)
            {
                if (ind > -1)
                {
                    List_Coin[i].Left = List_StatePos[ind].Left + (i - 1) * 50;
                    List_Coin[i].Top = 395;
                }
            }
            SelectStatePos = ind;
            
        }
        public void upgradeCoinSpeed()
        {
            CoinSpeedUp += 0.5f;
        }
        public int getSelectCoin(Vector2 pos)
        {
            for (int i = 0; i < List_Coin.Count; i++)
                if (List_Coin[i].IsValid && List_Coin[i].IsSelected(pos))
                    return i;
            return -1;
        }
        public int getFastPos(Vector2 pos)
        {
            for (int i = 0; i < List_Fast.Count; i++)
                if (List_Fast[i].IsSelected(pos))
                    return i;
            return -1;
        }
        public int getPausePos(Vector2 pos)
        {
            if (StateGame == gamestate.pause)
            {
                for (int i = 1; i < List_PauseGame.Count; i++)
                    if (List_PauseGame[i].IsSelected(pos))
                        return i;
            }
            if (StateGame == gamestate.turnback)
            {
                for (int i = List_PauseGame.Count-2; i < List_PauseGame.Count; i++)
                    if (List_PauseGame[i].IsSelected(pos))
                        return i;
            }
            return -1;
        }
        public int getStatePos(Vector2 pos)
        {
            for (int i = 0; i < List_StatePos.Count; i++)
                if (List_StatePos[i].IsSelected(pos))
                    return i;
            return -1;
        }
        public int getSkillPos(Vector2 pos)
        {
            if (List_Skill_Icon[0].IsSelected(pos) && countdown_windskill <= 0)
                return 0;
            if (List_Skill_Icon[1].IsSelected(pos) && countdown_iceskill <= 0)
                return 1;
            /*for (int i = 0; i < List_Skill_Icon.Count; i++)
                if (List_Skill_Icon[i].IsSelected(pos))
                    return i;*/
            return -1;
        }
        public int getUpgradePos(Vector2 pos)
        {
            for (int i = 0; i < List_UpGrade.Count; i++)
                if (List_UpGrade[i].IsSelected(pos) && EnemyFactory.getupdateEnemyPrice(i) <= Coin)
                    return i;
            return -1;
        }
        public void ExcuteSkill(int ind)
        {
            switch (ind)
            { 
                case 0: // Wind skill
                    countdown_windskill = 1600;
                    Skill_countdown = List_Skill[0].ListSprite1.Count;
                    Skill_Target = 0;
                    break;
                case 1: // Ice skill
                    countdown_iceskill = 1000;
                    Skill_countdown = List_Skill[1].ListSprite1.Count;
                    Skill_Target = 1;
                    break;
                default:
                    Skill_Target = -1;
                    Skill_countdown = -1;
                    break;
            }
            
        }
        public void pauseGame()
        {
            StateGame = gamestate.pause;
            showPauseMenu();
        }
        public void resumeGame()
        {
            StateGame = gamestate.playing;
        }
        public bool isPause(Vector2 pos)
        { 
            if(PauseSprite.IsSelected(pos))
                return true;
            return false;
        }
        public void upgradeEnemy(int type)
        {
            Coin -= EnemyFactory.getupdateEnemyPrice(type);
            EnemyFactory.updateEnemy(type);
        }
        public void CountdownTarget(int ind, int state)
        {
            TargetIndex.Add(ind);
            TargetPosState.Add(state);
            switch (ind)
            { 
                case 0:
                    time_countselect.Add(30);
                    Coin -= 200;
                    break;
                case 1:
                    time_countselect.Add(50);
                    Coin -= 500;
                    break;
                case 2:
                    time_countselect.Add(60);
                    Coin -= 700;
                    break;
                case 3:
                    time_countselect.Add(80);
                    Coin -= 1000;
                    break;
            }
            time_countselectMax.Add(time_countselect[time_countselect.Count - 1]);
        }
        public override void Draw(GameTime gameTime, SpriteBatch spritebatch)
        {
            //base.Draw(gameTime, obj);
            if(Global.CurrentMap != null)
                Global.CurrentMap.Draw(gameTime, spritebatch);

            spritebatch.End();
            spritebatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, Global.SubCamera.WVPMatrix);
            //xử lý màn hình
            if (CoinBar_Background != null)
                CoinBar_Background.Draw(gameTime, spritebatch);

            if (CoinBar != null)
            {
                spritebatch.Draw(CoinBar, new Rectangle(((int)CoinBar_Background.Left + 5), ((int)CoinBar_Background.Top + 5), CoinBar.Width, CoinBar.Height), Color.White);
                spritebatch.Draw(CoinBar, new Rectangle(((int)CoinBar_Background.Left + 5), ((int)CoinBar_Background.Top + 5), (int)(CoinBar.Width * Coin / Maxcoin), CoinBar.Height), Color.Yellow);
            }
            for (int i = 0; i < List_Coin.Count; i++)
                if (List_Coin[i] != null)
                {
                    if (List_Coin[i].IsValid)
                        List_Coin[i].Draw(gameTime, spritebatch);
                    else List_Coin[i].Draw(gameTime, spritebatch,Color.Gray);
                }
            for (int i = 0; i < List_StatePos.Count; i++)
                if (List_StatePos[i] != null)
                    List_StatePos[i].Draw(gameTime, spritebatch);
            for (int i = 0; i < List_UpGrade.Count; i++)
                if (List_UpGrade[i] != null)
                {
                    if (EnemyFactory.getupdateEnemyPrice(i) == -1)
                    {
                        List_UpGrade[i].Draw(gameTime, spritebatch, Color.Transparent);
                    }
                    else
                    {
                        if (EnemyFactory.getupdateEnemyPrice(i) <= Coin)
                            List_UpGrade[i].Draw(gameTime, spritebatch);
                        else List_UpGrade[i].Draw(gameTime, spritebatch, Color.Gray);
                    }
                }



            if (countdown_iceskill > 0)
            {
                List_Skill_Icon[1].Draw(gameTime, spritebatch, Color.Gray);
                spritebatch.Draw(CountDownBar, new Rectangle(((int)List_Skill_Icon[1].Left + 43), ((int)List_Skill_Icon[1].Top + 5), (int)(CountDownBar.Width), (int)(CountDownBar.Height * countdown_iceskill / 1000)), Color.Green);
                
            }
            else List_Skill_Icon[1].Draw(gameTime, spritebatch);
            if (countdown_windskill > 0)
            {
                List_Skill_Icon[0].Draw(gameTime, spritebatch, Color.Gray);
                spritebatch.Draw(CountDownBar, new Rectangle(((int)List_Skill_Icon[0].Left + 43), ((int)List_Skill_Icon[0].Top + 5), (int)(CountDownBar.Width), (int)(CountDownBar.Height * countdown_windskill / 1600)), Color.Green);
                
            }
            else List_Skill_Icon[0].Draw(gameTime, spritebatch);
            if (Skill_countdown > 0)
            {
                switch (Skill_Target)
                { 
                    case 0:
                        List_Skill[0].Draw(gameTime, spritebatch);
                        break;
                    case 1:
                        List_Skill[1].Draw(gameTime, spritebatch);
                        break;
                }
            }

            for (int i = 0; i < time_countselect.Count; i++)
            {
                if(time_countselect[i] != 0)
                if (time_countselect[i] > 0 && SelectStatePos == TargetPosState[i])
                    spritebatch.Draw(CountDownBar, new Rectangle(((int)List_Coin[TargetIndex[i]].Left + 43), ((int)List_Coin[TargetIndex[i]].Top + 5), (int)(CountDownBar.Width), (int)(CountDownBar.Height * time_countselect[i] / time_countselectMax[i])), Color.Red);
            }

            for (int i = 0; i < List_Fast.Count; i++)
                List_Fast[i].Draw(gameTime, spritebatch);
            if (PauseSprite != null)
                PauseSprite.Draw(gameTime, spritebatch);

            if (StateGame == gamestate.pause || StateGame == gamestate.turnback)
            {
                for (int i = 0; i < List_PauseGame.Count; i++)
                    List_PauseGame[i].Draw(gameTime, spritebatch);
            }

            spritebatch.End();
            spritebatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, Global.CurrentCamera.WVPMatrix);

        }


    }
}

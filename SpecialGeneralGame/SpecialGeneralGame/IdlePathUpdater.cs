﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class IdlePathUpdater : PathUpdater
    {
        float X0, Y0;
        public IdlePathUpdater(float x0, float y0)
        {
            X0 = x0;
            Y0 = y0;
        }
        public override Vector2 GetCurrentPosition()
        {
            return new Vector2(X0, Y0);
        }
    }
}

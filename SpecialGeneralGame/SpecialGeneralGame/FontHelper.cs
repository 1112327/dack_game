﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class FontHepler : VisibleGameEntity
    {
        float _left, _top;
        String _value;

        public String Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public float Top
        {
            get { return _top; }
            set { _top = value; }
        }

        public float Left
        {
            get { return _left; }
            set { _left = value; }
        }
        SpriteFont spritefont;

        public SpriteFont Spritefont
        {
            get { return spritefont; }
            set { spritefont = value; }
        }

        public void updatePos(Vector2 tran)
        {
            Left -= tran.X;
            Top -= tran.Y;
        }

        public FontHepler(float left, float top, String value, SpriteFont font)
        {
            Left = left;
            Top = top;
            spritefont = font;
            Value = value;
        }
        public override void Draw(GameTime gameTime, SpriteBatch obj)
        {
            obj.DrawString(spritefont, Value, new Vector2(Left, Top), Color.Black);
        }
        public void Draw(GameTime gameTime, SpriteBatch obj, Color color)
        {
            obj.DrawString(spritefont, Value, new Vector2(Left, Top), color);
        }
    }
}

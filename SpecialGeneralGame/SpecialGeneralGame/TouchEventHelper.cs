﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class TouchEventHelper : InvisibleGameEntity
    {
        protected TouchCollection CurrentState;
        protected TouchCollection PreviousState;
        public enum TypeTouch
        { SingleTouch, MultiTouch };

        public TypeTouch TouchType = TypeTouch.SingleTouch;
        private TouchEventHelper() { }
        private static TouchEventHelper Instance = new TouchEventHelper();
        public static TouchEventHelper GetInstance()
        { return Instance; }

        public void ProcessNewState(TouchCollection touchCollection, TouchPanelCapabilities Capabilities)
        {
            PreviousState = CurrentState;
            CurrentState = touchCollection;
            if (Capabilities.IsConnected)
            {
                if (Capabilities.MaximumTouchCount > 1)
                    TouchType = TypeTouch.MultiTouch;
                else TouchType = TypeTouch.SingleTouch;
                }
        }

        public bool HasTouchDownEvent()
        {
            foreach (TouchLocation locat in CurrentState)
            {
                if (locat.State == TouchLocationState.Pressed)
                    return true;  
               
            }
            return false;
        }
        public bool HasMoveDownEvent()
        {
            foreach (TouchLocation locat in CurrentState)
            {
                if (locat.State == TouchLocationState.Moved)
                    return true;
            }
            return false;
        }
        public bool HasTouchUpEvent()
        {
            foreach (TouchLocation locat in CurrentState)
            {
                if (locat.State == TouchLocationState.Released)
                    return true;
            }
            return false;
        }

        public Vector2 GetTouchPosition()
        {
            foreach (TouchLocation locat in CurrentState)
            {
                return locat.Position;
            }
            return Vector2.Zero ;
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            ProcessNewState(TouchPanel.GetState(), TouchPanel.GetCapabilities());
        }
        public Vector2 GetTouchPosDifference()
        {
            foreach (TouchLocation locat in CurrentState)
            {
                foreach (TouchLocation locat1 in PreviousState)
                    return new Vector2(locat1.Position.X - locat.Position.X, locat1.Position.Y - locat.Position.Y);
            }
            return Vector2.Zero;
        }
        public bool IsTouchDown()
        {
            foreach (TouchLocation locat in CurrentState)
                return locat.State == TouchLocationState.Pressed;
            return false;
        }
        public bool IsTouchUp()
        {
            foreach (TouchLocation locat in CurrentState)
                return locat.State == TouchLocationState.Released;
            return false;
        }
    }
}

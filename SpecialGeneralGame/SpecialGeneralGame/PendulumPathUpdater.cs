﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class PendulumPathUpdater : PathUpdater
    {
        float X0, Y0;
        float A, W, Phi, dA;
        float X, Y, t;
        static float epsilon = 0.1f;
        public PendulumPathUpdater(float x0, float y0, float vx, float vy, float a, float w, float phi, float da)
        {
            X0 = x0;
            Y0 = y0;
            A = a;
            W = w;
            Phi = phi;
            dA = da;
        }
        public override Vector2 GetCurrentPosition()
        {
            return new Vector2(X, Y);
        }
        public override void Update(GameTime gameTime)
        {
            t++;
            X = X0;
            Y = (float)(Y0 + A * Math.Sin(W * t + Phi));
            if (dA != 0)
            {
                A -= dA;
                if (Math.Abs(A) < epsilon)
                {
                    dA = 0;
                    A = 0;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public class LoadingForm : Form
    {
        int time = 100;
        int _nextFormID = 0;

        public int NextFormID
        {
            get { return _nextFormID; }
            set { _nextFormID = value;
            time = 100; 
            }
        }
        public LoadingForm()
        {
            List<Texture2D> texture = new List<Texture2D>();
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading1"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading2"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading3"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading4"));
            MainModel = new Sprite2D(0, 0, texture);
        }
        public LoadingForm(int next)
        {
            NextFormID = next;
            List<Texture2D> texture = new List<Texture2D>();
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading1"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading2"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading3"));
            texture.Add(Global.Content.Load<Texture2D>("Menu\\Loading4"));
            MainModel = new Sprite2D(0, 0, texture);
            
        }
        public override void Update(GameTime gameTime)
        {
            time--;
            if (time % 15 == 0)
                MainModel.Update(gameTime);
            if (time < 0)
            {
                if (Global.isMusic)
                {
                    if (NextFormID == 0)
                        BackgroundMusicHelper.GetInstance().playSong(1);
                    if (NextFormID == 1)
                        BackgroundMusicHelper.GetInstance().playSong(2);
                }
                Global.SwitchToForm(NextFormID);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class Boss : Building
    {
        List<Texture2D> walking, fight;
        int countdown_time = -1;
        float _damage;

        public float Damage
        {
          get { return _damage; }
          set { _damage = value; }
        }
        public enum state
        { 
            walking, fight
        }
        private state _state;

        public state State
        {
            get { return _state; }
            set { _state = value;
                switch (_state)
                { 
                    case state.walking:
                        MainModel.ListSprite1 = walking;
                        break;
                    case state.fight:
                        MainModel.ListSprite1 = fight;
                        break;
                }
            }
        }
        public Boss(bool isEnemy, int type)
            : base(isEnemy)
        {
            Health = 2500 + type * 2500;
            MaxHealth = 2500 + type * 2500;
            Damage = 4 * type;
            this.isEnemy = isEnemy;
            Type = type;
            walking = new List<Texture2D>();
            fight = new List<Texture2D>();
            List<Texture2D> tex = new List<Texture2D>();
            if (isEnemy)
            {
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_walking4"));
                MainModel = new Sprite2D(1400, 130, walking);
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight7"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight7"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight7"));
                fight.Add(Global.Content.Load<Texture2D>("Boss\\Boss1_fight7"));
            }
            else
            {
                
            }
            HealthBar = Global.Content.Load<Texture2D>("Scenario\\HealthBar_House");
        }
        public bool canChangeMode()
        {
            if (isEnemy)
            {
                for (int i = 0; i < Global.CurrentMap.myEnemy.Count; i++)
                    if (Global.CurrentMap.myEnemy[i] != null)
                        if (Global.CurrentMap.myEnemy[i].MainModel.Left + Global.CurrentMap.myEnemy[i].MainModel.Width + 20 > MainModel.Left)
                            return true;
                for (int i = 0; i < Global.CurrentMap.myEnemyUp.Count; i++)
                    if (Global.CurrentMap.myEnemyUp[i] != null)
                        if (Global.CurrentMap.myEnemyUp[i].MainModel.Left + Global.CurrentMap.myEnemyUp[i].MainModel.Width + 20 > MainModel.Left)
                            return true;
                for (int i = 0; i < Global.CurrentMap.myEnemyDown.Count; i++)
                    if (Global.CurrentMap.myEnemyDown[i] != null)
                        if (Global.CurrentMap.myEnemyDown[i].MainModel.Left + Global.CurrentMap.myEnemyDown[i].MainModel.Width + 20 > MainModel.Left)
                            return true;
            }
            return false;
        }
        public void ChangeMode()
        {
            State = state.fight;
            countdown_time = fight.Count;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            countdown_time--;
            if (countdown_time == 0)
            {
                MakeDamage(Damage);
                State = state.walking;
                countdown_time = -1;
            }
        }
        private void MakeDamage(float damage)
        {
            if (isEnemy)
            {
                for (int i = 0; i < Global.CurrentMap.myEnemy.Count; i++)
                    if (Global.CurrentMap.myEnemy[i] != null)
                        if (Global.CurrentMap.myEnemy[i].MainModel.Left + Global.CurrentMap.myEnemy[i].MainModel.Width + 20 > MainModel.Left)
                            Global.CurrentMap.myEnemy[i].TakeDamage(damage);
                for (int i = 0; i < Global.CurrentMap.myEnemyUp.Count; i++)
                    if (Global.CurrentMap.myEnemyUp[i] != null)
                        if (Global.CurrentMap.myEnemyUp[i].MainModel.Left + Global.CurrentMap.myEnemyUp[i].MainModel.Width + 20 > MainModel.Left)
                            Global.CurrentMap.myEnemyUp[i].TakeDamage(damage);
                for (int i = 0; i < Global.CurrentMap.myEnemyDown.Count; i++)
                    if (Global.CurrentMap.myEnemyDown[i] != null)
                        if (Global.CurrentMap.myEnemyDown[i].MainModel.Left + Global.CurrentMap.myEnemyDown[i].MainModel.Width + 20 > MainModel.Left)
                            Global.CurrentMap.myEnemyDown[i].TakeDamage(damage);
            }
           
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            /*if (Health > 0)
            {
                if (!isEnemy)
                {
                    spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), HealthBar.Width, HealthBar.Height), Color.Gray);
                    if (Health / MaxHealth <= 0.25)
                        spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Red);
                    else spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Green);
                }
                else
                {
                    spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height - 30), HealthBar.Width, HealthBar.Height), Color.Gray);
                    if (Health / MaxHealth <= 0.25)
                        spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height - 30), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Red);
                    else spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height - 30), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Green);

                }
            }*/
        }
    }
}

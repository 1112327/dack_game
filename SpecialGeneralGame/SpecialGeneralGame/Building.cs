﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class Building : VisibleGameEntity
    {
        protected bool isEnemy;
        float _health, _maxHealth;
        protected Texture2D HealthBar;
        private int _type;

        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public float MaxHealth
        {
            get { return _maxHealth; }
            set { _maxHealth = value; }
        }

        public float Health
        {
            get { return _health; }
            set { _health = value; }
        }
        public Building(bool isEnemy)
        {
            Health = 1000;
            MaxHealth = 1000;
            this.isEnemy = isEnemy;
            Type = 0;
            List<Texture2D> tex = new List<Texture2D>();
            if (!isEnemy)
            {
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\MainHouse"));
                MainModel = new Sprite2D(-100, 150, tex);
            }
            else
            {
                tex.Add(Global.Content.Load<Texture2D>("Scenario\\MainHouse_Enemy"));
                MainModel = new Sprite2D(1400, 150, tex);
            }
            HealthBar = Global.Content.Load<Texture2D>("Scenario\\HealthBar_House");
        }

        public void TakeDamage(float dam)
        {
            Health -= dam;
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (Health > 0)
            {
                if (!isEnemy)
                {
                    spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), HealthBar.Width, HealthBar.Height), Color.Gray);
                    if (Health / MaxHealth <= 0.25)
                        spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Red);
                    else spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 110), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Green);
                }
                else
                {
                    spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height), HealthBar.Width, HealthBar.Height), Color.Gray);
                    if (Health / MaxHealth <= 0.25)
                        spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Red);
                    else spriteBatch.Draw(HealthBar, new Rectangle(((int)MainModel.Left + 60), ((int)MainModel.Top + MainModel.Height), (int)(HealthBar.Width * Health / MaxHealth), HealthBar.Height), Color.Green);
                
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}

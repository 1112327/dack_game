﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public class WariorEnemy : Enemy
    {

        public WariorEnemy(bool isEnemy, Vector2 pos, position_state posstate)
            : base(isEnemy, pos, posstate)
        {
            if (!isEnemy)
            {
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_fight3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy1_die3"));
                Type = 0;
            }
            else
            {
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_fight3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy1_die3"));     
                Type = 4;
            }
            deadcount = die.Count;
            fightcount = fight.Count;
            MainModel = new Sprite2D(pos.X - walking[0].Width, pos.Y - walking[0].Height, walking);
            Health = 20;
            Maxhealth = 20;
            Speed = 5;
            Damage = 6;
        }

        public WariorEnemy(WariorEnemy wariorEnemy): base(wariorEnemy)
        {
            Health = wariorEnemy.Health;
            Speed = wariorEnemy.Speed;
            Damage = wariorEnemy.Damage;
            Type = wariorEnemy.Type;
            isEnemy = wariorEnemy.isEnemy;
            HealthBar = wariorEnemy.HealthBar;
            Maxhealth = wariorEnemy.Maxhealth;
            walking = wariorEnemy.walking;
            fight = wariorEnemy.fight;
            die = wariorEnemy.die;
            deadcount = wariorEnemy.deadcount;
            fightcount = wariorEnemy.fightcount;
            Position_State = wariorEnemy.Position_State;
            this.MainModel = new Sprite2D(wariorEnemy.MainModel.Left, wariorEnemy.MainModel.Top, wariorEnemy.MainModel.ListSprite1);

        }
        public override Enemy Clone()
        {
            return new WariorEnemy(this);
        }
    }
}

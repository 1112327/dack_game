﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpecialGeneralGame
{
    public class Form : VisibleGameEntity
    {
        private int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
    }
}

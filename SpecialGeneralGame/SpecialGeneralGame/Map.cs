﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using XmlData;





namespace SpecialGeneralGame
{
    public class Map : VisibleGameEntity
    {
        XmlData.Map CurrentMapData;

        public List<Enemy> myEnemy = new List<Enemy>();
        public List<Enemy> myEnemyUp = new List<Enemy>();
        public List<Enemy> myEnemyDown = new List<Enemy>();
        public List<Enemy> Enemy = new List<Enemy>();
        public List<Enemy> EnemyUp = new List<Enemy>();
        public List<Enemy> EnemyDown = new List<Enemy>();
        public List<Enemy> DeadEnemy = new List<Enemy>();
        public Building MainHouse, MainHouse_Enemy;
        public Map(int MapNum,string strtexture)
        {
            switch (MapNum)
            { 
                case 0:
                    CurrentMapData = Global.Content.Load<MapData>(strtexture).Maps[2];
                    List<Texture2D> tex = new List<Texture2D>();
                    tex.Add(Global.Content.Load<Texture2D>("Scenario\\Map01"));
                    MainModel = new Sprite2D(0, 0, tex);
                    MainHouse = new Building(false);
                    MainHouse_Enemy = new Boss(true, 1);
                    break;
            }
        }

        int waveNum = 0,enemy = 0;
        int toTime = 240;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (MainHouse_Enemy.Type == 1) //dang choi voi boss
            {
                if (((Boss)MainHouse_Enemy).canChangeMode() && ((Boss)MainHouse_Enemy).State == Boss.state.walking)
                    ((Boss)MainHouse_Enemy).ChangeMode();
            }
            toTime--;
            if (toTime <= 0)
            {
                enemy++;
                if (CurrentMapData.Waves[waveNum].Enemies.Count+1 == enemy) // doi wave
                {
                    waveNum++;
                    enemy = 0;
                    toTime = 420;
                    if (waveNum == CurrentMapData.Waves.Count) // het so wave
                    {
                        toTime = 600;
                        waveNum = 0;
                    }
                    else // chua het
                    {
                        //myEnemy.Add(EnemyFactory.BuildEnemy(CurrentMapData.Waves[waveNum].Enemies[enemy-1], false));
                        
                    }
                    
                }
                else
                {
                    
                    //myEnemy.Add(EnemyFactory.BuildEnemy(CurrentMapData.Waves[waveNum].Enemies[enemy - 1], false));
                    switch (CurrentMapData.Waves[waveNum].StatePos[enemy - 1])
                    {
                        case 0:
                            EnemyUp.Add(EnemyFactory.BuildEnemy(CurrentMapData.Waves[waveNum].Enemies[enemy - 1] + 8, true));
                            break;
                        case 1:
                            Enemy.Add(EnemyFactory.BuildEnemy(CurrentMapData.Waves[waveNum].Enemies[enemy - 1], true));
                            break;
                        case 2:
                            EnemyDown.Add(EnemyFactory.BuildEnemy(CurrentMapData.Waves[waveNum].Enemies[enemy - 1] + 16, true));
                            break;
                    }

                    toTime = 20;
                    
                }
            }
            if (toTime % 5 == 0)
            {
                for (int i = 0; i < Enemy.Count; i++)
                    if (Enemy[i] != null)
                    {
                        if (Enemy[i].countdown_slow > 0)
                        {
                            Enemy[i].countdown_slow--;
                            Enemy[i].slowdown_time--;
                            if (Enemy[i].slowdown_time == 0)
                            {
                                Enemy[i].Update(gameTime);
                                Enemy[i].slowdown_time = 3;
                            }
                        }
                        else Enemy[i].Update(gameTime);
                    }
                for(int i=0; i< EnemyUp.Count; i++)
                    if (EnemyUp[i] != null)
                    {
                        if (EnemyUp[i].countdown_slow > 0)
                        {
                            EnemyUp[i].countdown_slow--;
                            EnemyUp[i].slowdown_time--;
                            if (EnemyUp[i].slowdown_time == 0)
                            {
                                EnemyUp[i].Update(gameTime);
                                EnemyUp[i].slowdown_time = 3;
                            }
                        }
                        else EnemyUp[i].Update(gameTime);
                    }
                for (int i = 0; i < EnemyDown.Count; i++)
                    if (EnemyDown[i] != null)
                    {
                        if (EnemyDown[i].countdown_slow > 0)
                        {
                            EnemyDown[i].countdown_slow--;
                            EnemyDown[i].slowdown_time--;
                            if (EnemyDown[i].slowdown_time == 0)
                            {
                                EnemyDown[i].Update(gameTime);
                                EnemyDown[i].slowdown_time = 3;
                            }
                        }
                        else EnemyDown[i].Update(gameTime);
                    }

                for (int i = 0; i < myEnemy.Count; i++)
                    if (myEnemy[i] != null)
                        myEnemy[i].Update(gameTime);
                for (int i = 0; i < myEnemyUp.Count; i++)
                    if (myEnemyUp[i] != null)
                        myEnemyUp[i].Update(gameTime);
                for (int i = 0; i < myEnemyDown.Count; i++)
                    if (myEnemyDown[i] != null)
                        myEnemyDown[i].Update(gameTime);
                
                for (int i = 0; i < DeadEnemy.Count; i++)
                    if (DeadEnemy[i] != null)
                        DeadEnemy[i].Update(gameTime);
            }


            if (MainHouse != null)
                MainHouse.Update(gameTime);
            if (MainHouse_Enemy != null)
                MainHouse_Enemy.Update(gameTime);
        }
        public void ExcuteSkill(int ind)
        {
            switch (ind)
            {
                case 0:
                    {
                        for (int i = 0; i < Enemy.Count; i++)
                            if (Enemy[i] != null)
                                Enemy[i].TakeDamage(60);
                        for (int i = 0; i < EnemyUp.Count; i++)
                            if (EnemyUp[i] != null)
                                EnemyUp[i].TakeDamage(60);
                        for (int i = 0; i < EnemyDown.Count; i++)
                            if (EnemyDown[i] != null)
                                EnemyDown[i].TakeDamage(60);
                    }
                    break;
                case 1: // slow down enemy
                    {
                        for (int i = 0; i < Enemy.Count; i++)
                            if (Enemy[i] != null)
                            {
                                Enemy[i].TakeDamage(10);
                                Enemy[i].SlowDown(120);
                            }
                        for (int i = 0; i < EnemyUp.Count; i++)
                            if (EnemyUp[i] != null)
                            {
                                EnemyUp[i].TakeDamage(10);
                                EnemyUp[i].SlowDown(120);
                            }
                        for (int i = 0; i < EnemyDown.Count; i++)
                            if (EnemyDown[i] != null)
                            {
                                EnemyDown[i].TakeDamage(10);
                                EnemyDown[i].SlowDown(120);
                            }

                    }
                    break;
            }
        }
        public void buyEnemy(int type)
        {
            myEnemy.Add(EnemyFactory.BuildEnemy(type, false));
            
        }
        public void buyEnemy(int type, int state)
        {
            switch (state)
            { 
                case 0:
                    myEnemyUp.Add(EnemyFactory.BuildEnemy(type+8, false));
                    break;
                case 1:
                    myEnemy.Add(EnemyFactory.BuildEnemy(type, false));
                    break;
                case 2:
                    myEnemyDown.Add(EnemyFactory.BuildEnemy(type+16, false));
                    break;
            }
        }
        public override void Draw(GameTime gameTime, SpriteBatch spritebatch)
        {
            base.Draw(gameTime, spritebatch);

            for (int i = 0; i < Enemy.Count; i++)
                if (Enemy[i] != null)
                    Enemy[i].Draw(gameTime, spritebatch);
            for (int i = 0; i < EnemyUp.Count; i++)
                if (EnemyUp[i] != null)
                    EnemyUp[i].Draw(gameTime, spritebatch);
            for (int i = 0; i < EnemyDown.Count; i++)
                if (EnemyDown[i] != null)
                    EnemyDown[i].Draw(gameTime, spritebatch);

            for(int i=0;i<myEnemy.Count;i++)
                if (myEnemy[i] != null)
                    myEnemy[i].Draw(gameTime, spritebatch);
            for (int i = 0; i < myEnemyUp.Count; i++)
                if (myEnemyUp[i] != null)
                    myEnemyUp[i].Draw(gameTime, spritebatch);
            for (int i = 0; i < myEnemyDown.Count; i++)
                if (myEnemyDown[i] != null)
                    myEnemyDown[i].Draw(gameTime, spritebatch);
            
            for (int i = 0; i < DeadEnemy.Count; i++)
                if (DeadEnemy[i] != null)
                    DeadEnemy[i].Draw(gameTime, spritebatch);
            if (MainHouse != null)
                MainHouse.Draw(gameTime, spritebatch);
            if (MainHouse_Enemy != null)
                MainHouse_Enemy.Draw(gameTime, spritebatch);
        }

        
        public override bool IsSelected(Vector2 obj)
        {
            return false;
        }
        public override void Select(bool bSelected)
        {
            //base.Select(bSelected);
        }
        public virtual Vector2 World2Screen(Vector2 worldPos)
        { 
            return Vector2.Transform(worldPos,Global.CurrentCamera.WVPMatrix);
        }
        public virtual Vector2 Screen2World(Vector2 screenPos)
        {
            return Vector2.Transform(screenPos, Global.CurrentCamera.InvWVPMatrix);
        }


        
    }
}

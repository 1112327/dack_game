﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public class MinotaurEnemy : Enemy
    {

        public MinotaurEnemy(bool isEnemy, Vector2 pos, position_state posstate)
            : base(isEnemy, pos, posstate)
        {
            if (!isEnemy)
            {
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_fight7"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die4"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy6_die4"));
                Type = 3;
            }
            else
            {
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_fight7"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die4"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy6_die4"));
                Type = 7;
            }
            deadcount = die.Count;
            fightcount = fight.Count;
            MainModel = new Sprite2D(pos.X - walking[0].Width, pos.Y - walking[0].Height, walking);
            Health = 80;
            Maxhealth = 80;
            Speed = 5;
            Damage = 15;
        }

        public MinotaurEnemy(MinotaurEnemy minotaurEnemy) : base(minotaurEnemy)
        {
            Health = minotaurEnemy.Health;
            Speed = minotaurEnemy.Speed;
            Damage = minotaurEnemy.Damage;
            Type = minotaurEnemy.Type;
            isEnemy = minotaurEnemy.isEnemy;
            HealthBar = minotaurEnemy.HealthBar;
            Maxhealth = minotaurEnemy.Maxhealth;
            walking = minotaurEnemy.walking;
            fight = minotaurEnemy.fight;
            die = minotaurEnemy.die;
            deadcount = minotaurEnemy.deadcount;
            fightcount = minotaurEnemy.fightcount;
            Position_State = minotaurEnemy.Position_State;
            this.MainModel = new Sprite2D(minotaurEnemy.MainModel.Left, minotaurEnemy.MainModel.Top, minotaurEnemy.MainModel.ListSprite1);
        }
        public override Enemy Clone()
        {
            return new MinotaurEnemy(this);
        }
    }
}

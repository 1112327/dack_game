﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;
using Microsoft.Phone.Tasks;

namespace SpecialGeneralGame
{
    public class IntroduceForm : Form
    {
        
        Texture2D  textTexture;
        public IntroduceForm()
        {
            MediaPlayerLauncher mediaPlayerLauncher = new MediaPlayerLauncher(); 
            mediaPlayerLauncher.Media = new Uri("Content/Video/Intro.wmv", UriKind.Relative);
            mediaPlayerLauncher.Location = MediaLocationType.Install;
            mediaPlayerLauncher.Controls = MediaPlaybackControls.Pause | MediaPlaybackControls.Stop;
            mediaPlayerLauncher.Show();
            

            List<Texture2D> tex = new List<Texture2D>();
            
            textTexture = Global.Content.Load<Texture2D>("Menu\\skip_intro");
            tex.Add(textTexture);
            MainModel = new Sprite2D(250, 0, tex);
        }

        public override void Update(GameTime gameTime)
        {

            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                Vector2 MousePosition = TouchEventHelper.GetInstance().GetTouchPosition();
                if (MainModel.IsSelected(MousePosition))
                {

                    Global.SwitchToForm(2);
                    ((LoadingForm)Global.CurrentForm).NextFormID = 0;

                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            // Drawing to the rectangle will stretch the 
            // video to fill the screen
            Rectangle screen = new Rectangle(Global.graphics.Viewport.X,Global.graphics.Viewport.Y,
                Global.graphics.Viewport.Width,Global.graphics.Viewport.Height);

            // Draw the video, if we have a texture to draw.
            
            base.Draw(gameTime, spriteBatch);
        }
    }
}

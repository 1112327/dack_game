﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class EnemyFactory
    {
        public static List<Enemy> Prototype = new List<Enemy>();
        static int[] updateCount = new int[5];
        static int[,] updateIndex = new int[5,3];
        static EnemyFactory() { Init(); }
        /*public static EnemyFactory getInstance()
        { 
            return 
        }*/
        private static void Init()
        {
            //duong giua
            Prototype.Add(new WariorEnemy(false, new Vector2(100, 380), Enemy.position_state.mid));
            Prototype.Add(new OneEyesEnemy(false, new Vector2(100, 380), Enemy.position_state.mid));
            Prototype.Add(new HyenasEnemy(false, new Vector2(100, 380), Enemy.position_state.mid));
            Prototype.Add(new MinotaurEnemy(false, new Vector2(100, 380), Enemy.position_state.mid));
            Prototype.Add(new WariorEnemy(true, new Vector2(1500, 380), Enemy.position_state.mid));
            Prototype.Add(new OneEyesEnemy(true, new Vector2(1500, 380), Enemy.position_state.mid));
            Prototype.Add(new HyenasEnemy(true, new Vector2(1500, 380), Enemy.position_state.mid));
            Prototype.Add(new MinotaurEnemy(true, new Vector2(1500, 380), Enemy.position_state.mid));
            //duong tren
            Prototype.Add(new WariorEnemy(false, new Vector2(100, 330), Enemy.position_state.up));
            Prototype.Add(new OneEyesEnemy(false, new Vector2(100, 330), Enemy.position_state.up));
            Prototype.Add(new HyenasEnemy(false, new Vector2(100, 330), Enemy.position_state.up));
            Prototype.Add(new MinotaurEnemy(false, new Vector2(100, 330), Enemy.position_state.up));
            Prototype.Add(new WariorEnemy(true, new Vector2(1500, 330), Enemy.position_state.up));
            Prototype.Add(new OneEyesEnemy(true, new Vector2(1500, 330), Enemy.position_state.up));
            Prototype.Add(new HyenasEnemy(true, new Vector2(1500, 330), Enemy.position_state.up));
            Prototype.Add(new MinotaurEnemy(true, new Vector2(1500, 330), Enemy.position_state.up));
            //duong duoi
            Prototype.Add(new WariorEnemy(false, new Vector2(100, 430), Enemy.position_state.down));
            Prototype.Add(new OneEyesEnemy(false, new Vector2(100, 430), Enemy.position_state.down));
            Prototype.Add(new HyenasEnemy(false, new Vector2(100, 430), Enemy.position_state.down));
            Prototype.Add(new MinotaurEnemy(false, new Vector2(100, 430), Enemy.position_state.down));
            Prototype.Add(new WariorEnemy(true, new Vector2(1500, 430), Enemy.position_state.down));
            Prototype.Add(new OneEyesEnemy(true, new Vector2(1500, 430), Enemy.position_state.down));
            Prototype.Add(new HyenasEnemy(true, new Vector2(1500, 430), Enemy.position_state.down));
            Prototype.Add(new MinotaurEnemy(true, new Vector2(1500, 430), Enemy.position_state.down));

            //them gia tien update enemy
            
            for(int i=0; i<4; i++)
            {
                updateCount[i] = 0;
                for (int j = 0; j < 3; j ++)
                    updateIndex[i, j] = (i+1) * 100 + (j+1) * 200;
            }
            updateCount[4] = 0;
            updateIndex[4, 0] = 600;
            updateIndex[4, 1] = 1200;
            updateIndex[4, 2] = 1800;
        }
        public static Enemy BuildEnemy(int type, bool isEnemy)
        {
            if (!isEnemy)
                return Prototype[type].Clone();
            else return Prototype[type + 4].Clone();
        }
        public static int getupdateEnemyPrice(int type)
        {
            if (updateCount[type] >= 3)
            {
                return -1;
            }
            return updateIndex[type, updateCount[type]];
        }
        public static void updateEnemy(int type)
        {
            updateCount[type]++;
            updateEnemyTrain(updateCount[type], type);

        }
        private static void updateEnemyTrain(int times,int type)
        {
            if (type <= 3)
            {
                switch (type)
                {
                    case 0: // update enemy loai 1
                        {
                            switch (times)
                            {
                                case 1:
                                    Prototype[type].Maxhealth += 10;
                                    Prototype[type].Health += 10;
                                    Prototype[type].Damage += 2;
                                    break;
                                case 2:
                                    Prototype[type].Maxhealth += 15;
                                    Prototype[type].Health += 15;
                                    Prototype[type].Damage += 2;
                                    break;
                                case 3:
                                    Prototype[type].Maxhealth += 20;
                                    Prototype[type].Health += 20;
                                    Prototype[type].Damage += 2;
                                    break;
                            }
                        }
                        break;
                    case 1: // update enemy loai 2
                        {
                            switch (times)
                            {
                                case 1:
                                    Prototype[type].Maxhealth += 20;
                                    Prototype[type].Health += 20;
                                    Prototype[type].Damage += 3;
                                    break;
                                case 2:
                                    Prototype[type].Maxhealth += 30;
                                    Prototype[type].Health += 30;
                                    Prototype[type].Damage += 3;
                                    break;
                                case 3:
                                    Prototype[type].Maxhealth += 40;
                                    Prototype[type].Health += 40;
                                    Prototype[type].Damage += 3;
                                    break;
                            }
                        }
                        break;
                    case 2: // update enemy loai 3
                        {
                            switch (times)
                            {
                                case 1:
                                    Prototype[type].Maxhealth += 25;
                                    Prototype[type].Health += 25;
                                    Prototype[type].Damage += 4;
                                    break;
                                case 2:
                                    Prototype[type].Maxhealth += 40;
                                    Prototype[type].Health += 40;
                                    Prototype[type].Damage += 4;
                                    break;
                                case 3:
                                    Prototype[type].Maxhealth += 55;
                                    Prototype[type].Health += 55;
                                    Prototype[type].Damage += 4;
                                    break;
                            }
                        }
                        break;
                    case 3: // update enemy loai 4
                        {
                            switch (times)
                            {
                                case 1:
                                    Prototype[type].Maxhealth += 30;
                                    Prototype[type].Health += 30;
                                    Prototype[type].Damage += 5;
                                    break;
                                case 2:
                                    Prototype[type].Maxhealth += 50;
                                    Prototype[type].Health += 50;
                                    Prototype[type].Damage += 5;
                                    break;
                                case 3:
                                    Prototype[type].Maxhealth += 70;
                                    Prototype[type].Health += 70;
                                    Prototype[type].Damage += 5;
                                    break;
                            }
                        }
                        break;

                }
                Prototype[type + 8].Maxhealth = Prototype[type].Maxhealth;
                Prototype[type + 8].Health = Prototype[type].Health;
                Prototype[type + 8].Damage = Prototype[type].Damage;
                Prototype[type + 16].Maxhealth = Prototype[type].Maxhealth;
                Prototype[type + 16].Health = Prototype[type].Health;
                Prototype[type + 16].Damage = Prototype[type].Damage;
            }
            else // update toc do tang tien
            {
                ((GameForm)Global.CurrentForm).upgradeCoinSpeed();
            }
       }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class Camera : InvisibleGameEntity
    {
        private PathUpdater _pathupdater;

        public PathUpdater Pathupdater
        {
            get { return _pathupdater; }
            set { _pathupdater = value; }
        }
        protected Matrix _World = Matrix.Identity, _View = Matrix.Identity, _Projection = Matrix.Identity;

        public Matrix Projection
        {
            get { return _Projection; }
            set { _Projection = value; }
        }

        public Matrix View
        {
            get { return _View; }
            set { _View = value; }
        }

        public Matrix World
        {
            get { return _World; }
            set { _World = value; }
        }
        public Matrix WVPMatrix // World -> Screen
        {
            get { return World * View * Projection; }
        }

        public Matrix InvWVPMatrix
        {
            get { return Matrix.Invert(WVPMatrix); }
        }

        float xScale, yScale, zRot, xTran, yTran;

        public float YScale
        {
            get { return yScale; }
            set { yScale = value; }
        }

        public float XScale
        {
            get { return xScale; }
            set { xScale = value; }
        }

        public float YTran
        {
            get { return yTran; }
            set { yTran = value; }
        }

        public float XTran
        {
            get { return xTran; }
            set { xTran = value; }
        }
        bool isUsed = false;
        public Camera(float xscale, float yscale, float zrot, float xtran, float ytran)
        {
            xScale = xscale;
            yScale = yscale;
            zRot = zrot;
            xTran = xtran;
            yTran = ytran;
            isUsed = true;
        }
        public override void Update(GameTime gameTime)
        {
            if (Pathupdater != null)
            {
                Pathupdater.Update(gameTime);
                Vector2 CurPos = Pathupdater.GetCurrentPosition();
            }
            View = Matrix.CreateScale(xScale, yScale, 1) *
                Matrix.CreateRotationZ(zRot) *
                Matrix.CreateTranslation(new Vector3(xTran, yTran, 0));
            
        }
        public bool IsUsed()
        {
            return isUsed;
        }
        public void Used(bool used)
        {
            isUsed = used;
        }
    }
}

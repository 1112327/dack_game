﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace SpecialGeneralGame
{
    public class Global
    {
        public enum difficult {
            easy,normal, hard
        };
        public static Camera DefaultCamera = null;
        public static difficult Difficult = difficult.easy;
        public static GraphicsDevice graphics = null;
        private static Random random = new Random();
        public static List<Form> Forms = new List<Form>();
        public static Camera CurrentCamera = null;
        public static Camera SubCamera = null;
        public static ContentManager Content = null;
        public static Map CurrentMap = null;
        public static Form CurrentForm = null;
        public static bool bQuit = false;
        public static Player player = null;
        public static List<int> Turns = new List<int>();
        public static bool isMusic = true;
        public static bool isSound = true;
        public static void LoadAllParametersFromFile(String file)
        { 
        }
        public static Form GetForm(int id)
        {
            for (int i = 0; i < Forms.Count; i++)
                if (Forms[i].ID == id)
                    return Forms[i];
            return null;
        }
        public static void createGameForm()
        {
            for (int i = 0; i < Forms.Count; i++)
                if (Forms[i].ID == 1)
                    Forms.Remove(Forms[i]);
            GameForm form = new GameForm(0);
            form.ID = 1;
            Forms.Add(form);
        }
        public static void convertCamera()
        {
            CurrentCamera = new Camera(1,1,0,0,0);
            SubCamera = new Camera(1,1,0,0,0); 
            
        }
        public static int SwitchToForm(int FormID)
        {
            int OldFormID = -1;
            if (CurrentForm != null)
                OldFormID = CurrentForm.ID;

            if (OldFormID == FormID)
                return OldFormID;

            for(int i=0; i<Forms.Count;i++)
                if (Forms[i].ID == FormID)
                {
                    
                    CurrentForm = Forms[i];
                    

                    break;
                }

            return OldFormID;
        }

        internal static Vector2 Screen2World(Vector2 MousePosition)
        {
            return MousePosition;
        }
        public static int getRanDom(int max)
        {
            return random.Next(max);
        }
       
        private static bool isInTurns(int ind)
        {
            for (int i = 0; i < Turns.Count; i++)
                if (Turns[i] == ind)
                    return true;
            return false;
        }
        
    }
}

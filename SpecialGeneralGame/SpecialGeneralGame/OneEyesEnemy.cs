﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public class OneEyesEnemy : Enemy
    {

        public OneEyesEnemy(bool isEnemy, Vector2 pos, position_state posstate)
            : base(isEnemy, pos, posstate)
        {
            if (!isEnemy)
            {
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_fight6"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy3_die3"));
                Type = 1;
            }
            else
            {
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_walking6"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_fight6"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy3_die3"));
                Type = 5;
            }
            deadcount = die.Count;
            fightcount = fight.Count;
            MainModel = new Sprite2D(pos.X - walking[0].Width, pos.Y - walking[0].Height, walking);
            Health = 40;
            Maxhealth = 40;
            Speed = 3;
            Damage = 8;
        }

        public OneEyesEnemy(OneEyesEnemy oneEyesEnemy) : base(oneEyesEnemy)
        {
            Health = oneEyesEnemy.Health;
            Speed = oneEyesEnemy.Speed;
            Damage = oneEyesEnemy.Damage;
            Type = oneEyesEnemy.Type;
            isEnemy = oneEyesEnemy.isEnemy;
            HealthBar = oneEyesEnemy.HealthBar;
            Maxhealth = oneEyesEnemy.Maxhealth;
            walking = oneEyesEnemy.walking;
            fight = oneEyesEnemy.fight;
            die = oneEyesEnemy.die;
            deadcount = oneEyesEnemy.deadcount;
            fightcount = oneEyesEnemy.fightcount;
            Position_State = oneEyesEnemy.Position_State;
            this.MainModel = new Sprite2D(oneEyesEnemy.MainModel.Left, oneEyesEnemy.MainModel.Top, oneEyesEnemy.MainModel.ListSprite1);
        }
        public override Enemy Clone()
        {
            return new OneEyesEnemy(this);
            //return base.Clone();
        }
    }
}

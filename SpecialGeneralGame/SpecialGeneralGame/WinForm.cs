﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class WinForm : Form
    {
        public WinForm()
        {
            LoadContent();
        }
        bool isWantExit = false;
        List<Sprite2D_Menu> sprites = new List<Sprite2D_Menu>();

        SpriteFont font = Global.Content.Load<SpriteFont>("Font\\Courier New");
        List<FontHepler> listFont = new List<FontHepler>();
        private void LoadContent()
        {
            CreateMenuBackgroundSprite(0, 0, "Menu\\victory");
            CreateMenuButton(280, 280, "Menu\\tieptuc");
            CreateMenuButton(200, 350, "Menu\\menu");
            CreateMenuButton(450, 350, "Menu\\thoat");
        }

        private void CreateMenuButton(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D_Menu menuButtonSprite = new Sprite2D_Menu(left, top, textures);
            sprites.Add(menuButtonSprite);
        }

    
        public void updateData()
        {
            listFont.Add(new FontHepler(400,155,((GameForm)Global.GetForm(7)).TotalTime.ToString(),font));
            listFont.Add(new FontHepler(400, 205, ((GameForm)Global.GetForm(7)).Score.ToString(), font));
        }
        private void CreateMenuBackgroundSprite(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D menuBackgroundSprite = new Sprite2D(left,top,textures);
            this.MainModel = menuBackgroundSprite;
            
        }
        public override void Update(GameTime gameTime)
        {

            //base.Update(gameTime);
            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                // duplicated code ==> should be optimized!!!
                Vector2 ScreenPos = TouchEventHelper.GetInstance().GetTouchPosition();
                Vector2 WorldPosition = Global.Screen2World(ScreenPos);

                int idx = getSelectButtonIndex(WorldPosition);
                if (idx != -1)
                {
                    for (int i = 0; i < sprites.Count; i++)
                    {
                        sprites[i].Select(i == idx);

                    }
                    if (Global.isSound) SoundEffectHelper.GetInstance().playSound(1);
                    switch (idx) // chuyen man hinh
                    {

                        case 0: //tiep tuc choi man tiep theo

                            break;

                        case 1: //ve menu

                            break;
                       
                        case 2:
                            showQuit();
                            break;

                        case 5:
                            Global.bQuit = true;
                            break;
                        case 6:
                            xoaQuit();
                            break;

                    }
                }
            }
        }

        private int getSelectButtonIndex(Vector2 WorldPosition)
        {
            int idx = -1;
            if (isWantExit == false)
            {
                for (int i = 0; i < sprites.Count; i++)
                    if (sprites[i].IsSelected(WorldPosition))
                    {
                        idx = i;
                        break;
                    }
            }
            else
                for (int i = sprites.Count - 2; i < sprites.Count; i++)
                    if (sprites[i].IsSelected(WorldPosition))
                    {
                        idx =i;
                        break;
                    }
            return idx;
        }
        public void showQuit()
        {
            isWantExit = true;
            List<Texture2D> tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\border_quit"));
            sprites.Add(new Sprite2D_Menu(220, 100, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\quit_question"));
            sprites.Add(new Sprite2D_Menu(320, 160, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\yes"));
            sprites.Add(new Sprite2D_Menu(300, 250, tex));
            tex = new List<Texture2D>();
            tex.Add(Global.Content.Load<Texture2D>("Menu\\no"));
            sprites.Add(new Sprite2D_Menu(450, 250, tex));
        }
        public void xoaQuit()
        {
            isWantExit = false;
            for (int i = 0; i < 4; i++)
                sprites.Remove(sprites[sprites.Count - 1]);
        }
        public override void Draw(GameTime gameTime, SpriteBatch spritebatch)
        {
           //spritebatch.End();
           //spritebatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, Global.SubCamera.WVPMatrix); 
           base.Draw(gameTime, spritebatch); // background
           for (int i = 0; i < listFont.Count; i++)
               listFont[i].Draw(gameTime, spritebatch, Color.Red);
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Draw(gameTime, spritebatch);
            
        }
    }
}

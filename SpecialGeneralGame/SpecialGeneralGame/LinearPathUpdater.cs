﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class LinearPathUpdater : PathUpdater
    {
        float X0, Y0;
        float Vx, Vy;
        float X, Y, t;
        public LinearPathUpdater(float x0, float y0, float vx, float vy)
        {
            X0 = x0;
            Y0 = y0;
            Vx = vx;
            Vy = vy;
        }
        public override Vector2 GetCurrentPosition()
        {
            return new Vector2(X, Y);
        }
        public override void Update(GameTime gameTime)
        {
            t++;
            X = X0 + Vx * t;
            Y = Y0 + Vy * t;
        }
    }
}

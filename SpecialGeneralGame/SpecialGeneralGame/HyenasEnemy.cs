﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public class HyenasEnemy : Enemy
    {


        public HyenasEnemy(bool isEnemy, Vector2 pos, position_state posstate)
            : base(isEnemy, pos, posstate)
        {
            if (!isEnemy)
            {
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking6"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking7"));
                walking.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_walking8"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_fight7"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die1"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die2"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die3"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die4"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die5"));
                die.Add(Global.Content.Load<Texture2D>("MyEnemy\\enemy7_die5"));
                Type = 2;
            }
            else
            {
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking1"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking2"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking3"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking4"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking5"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking6"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking7"));
                walking.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_walking8"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight1"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight2"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight3"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight4"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight5"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight6"));
                fight.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_fight7"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die1"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die2"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die3"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die4"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die5"));
                die.Add(Global.Content.Load<Texture2D>("Enemy\\enemy7_die5")); 
                Type = 6;
            }
            deadcount = die.Count;
            fightcount = fight.Count;
            MainModel = new Sprite2D(pos.X - walking[0].Width, pos.Y - walking[0].Height, walking);
            Health = 60;
            Maxhealth = 60;
            Speed = 6;
            Damage = 12;
        }

        public HyenasEnemy(HyenasEnemy hyenasEnemy) :base(hyenasEnemy)
        {
            Health = hyenasEnemy.Health;
            Speed = hyenasEnemy.Speed;
            Damage = hyenasEnemy.Damage;
            Type = hyenasEnemy.Type;
            isEnemy = hyenasEnemy.isEnemy;
            HealthBar = hyenasEnemy.HealthBar;
            Maxhealth = hyenasEnemy.Maxhealth;
            walking = hyenasEnemy.walking;
            fight = hyenasEnemy.fight;
            die = hyenasEnemy.die;
            deadcount = hyenasEnemy.deadcount;
            fightcount = hyenasEnemy.fightcount;
            Position_State = hyenasEnemy.Position_State;
            this.MainModel = new Sprite2D(hyenasEnemy.MainModel.Left, hyenasEnemy.MainModel.Top, hyenasEnemy.MainModel.ListSprite1);
        }
        public override Enemy Clone()
        {
            return new HyenasEnemy(this);
            //return base.Clone();
        }
    }
}

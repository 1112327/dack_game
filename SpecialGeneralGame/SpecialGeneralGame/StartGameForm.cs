﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpecialGeneralGame
{
    public class StartGameForm : Form
    {
        public StartGameForm()
        {
            LoadContent();
        }

        List<Sprite2D_Menu> sprites = new List<Sprite2D_Menu>();
        
        
        private void LoadContent()
        {
            CreateMenuBackgroundSprite(0, 0, "Menu\\background");
            CreateMenuButton(120, 250, "Menu\\easy");
            CreateMenuButton(300, 350, "Menu\\normal");
            CreateMenuButton(460, 450, "Menu\\hard");
            CreateMenuButton(300, 530, "Menu\\back");
        }

        private void CreateMenuButton(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D_Menu menuButtonSprite = new Sprite2D_Menu(left, top, textures);
            sprites.Add(menuButtonSprite);
        }

        private void CreateMenuBackgroundSprite(int left, int top, string strTexture)
        {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Global.Content.Load<Texture2D>(strTexture));
            Sprite2D menuBackgroundSprite = new Sprite2D(left,top,textures);
            this.MainModel = menuBackgroundSprite;
            
        }
        public override void Update(GameTime gameTime)
        {
            
            if (TouchEventHelper.GetInstance().HasTouchDownEvent())
            {
                // duplicated code ==> should be optimized!!!
                Vector2 MousePosition = TouchEventHelper.GetInstance().GetTouchPosition();
                Vector2 WorldPosition = Global.Screen2World(MousePosition);

                int idx = getSelectButtonIndex(WorldPosition);
                if (idx != -1)
                {
                    for (int i = 0; i < sprites.Count; i++)
                    {
                        sprites[i].Select(i == idx);
                        
                    }
                    if (Global.isSound) SoundEffectHelper.GetInstance().playSound(1);
                    switch (idx) // chuyen man hinh
                    { 
                        case 0:
                            break;
                    }
                }
            }
            else
           /*     if (MouseEventHelper.GetInstance().IsLeftButtonUp())
                {
                    Vector2 MousePosition = MouseEventHelper.GetInstance().GetMousePosition();
                    Vector2 WorldPosition = Global.Screen2World(MousePosition);

                    int idx = getSelectButtonIndex(WorldPosition);
                    if (idx != -1)
                    {
                        for (int i = 0; i < sprites.Count; i++)
                                sprites[i].Select(i == idx);
                        
                    }
            
                }*/
            base.Update(gameTime);  // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Update(gameTime);
        }

        private int getSelectButtonIndex(Vector2 WorldPosition)
        {
            int idx = -1;
            for (int i = 0; i < sprites.Count; i++)
                if (sprites[i].IsSelected(WorldPosition))
                {
                    idx = i;
                    break;
                }
            return idx;
        }
        public override void Draw(GameTime gameTime, SpriteBatch obj)
        {
            base.Draw(gameTime, obj); // background
            for (int i = 0; i < sprites.Count; i++)
                sprites[i].Draw(gameTime, obj);
            
        }
    }
}


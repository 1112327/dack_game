﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpecialGeneralGame
{
    public abstract class VisibleGameEntity : GameEntity
    {
        Sprite2D _MainModel;

        public Sprite2D MainModel
        {
            get { return _MainModel; }
            set { _MainModel = value; }
        }

        public override void Update(GameTime gameTime)
        {
            MainModel.Update(gameTime);
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch obj)
        {
            MainModel.Draw(gameTime, obj);
        }
        public virtual bool IsSelected(Vector2 obj)
        {
            return MainModel.IsSelected(obj);
        }

        public virtual void Select(bool bSelected)
        {
            MainModel.Select(bSelected);
        }
    }
}

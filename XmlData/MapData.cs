using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using System.Xml;


namespace XmlData
{
    public class MapData
    {
        public List<Map> Maps;
        public void WriteData()
        {

            
            /* XmlWriterSettings xmlSettings = new XmlWriterSettings();
             xmlSettings.Indent = true;


             using (XmlWriter xmlWriter = XmlWriter.Create("Data\\MapData.xml", xmlSettings))
             {
                 IntermediateSerializer.Serialize(xmlWriter, this, null);
             */
        }
    }
    public class Map
    {
 
        public List<Wave> Waves;
    }
    public class Wave
    {
        public List<int> Enemies;
        public List<int> StatePos;
    }
    public class MapDataContentReader : ContentTypeReader<MapData>
    {
        protected override MapData Read(ContentReader input, MapData existingInstance)
        {
            MapData data = new MapData();

            data.Maps = input.ReadObject<List<Map>>();

            return data;
        }
    }

   /* [ContentTypeWriter]
    public class MapDataContentWriter : ContentTypeWriter<MapData>
    {
        protected override void Write(ContentWriter output, MapData map)
        {
            output.WriteObject(map.Maps);
        }
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(MapDataContentWriter).AssemblyQualifiedName;

        }
        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(XmlDocument).AssemblyQualifiedName;
        }
    }*/
}
